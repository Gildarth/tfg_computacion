\chapter{Etapa inicial}
\minitoc
\label{chap:desarrollo}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Objetivo: Narrar el desarrollo del proyecto             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	La librería de \textit{qutip} ofrece una función que devuelve el operador para simular la transformada cuántica de Fourier, el cual se construye a partir de las puertas individuales del circuito cuántico y tiene como limitación un máximo de 12 qubits.
	
	El primer paso del desarrollo consiste en un estudio del consumo de recursos por parte de esta implementación con el fin de poder comparar luego los resultados de las implementaciones realizadas.
	
	Con ello, este proyecto tiene por objetivo final la creación de una función que ofrezca la operación de la transformada cuántica de Fourier para estados de mayor tamaño, generando el operador mediante la aplicación directa de la formula matemática en lugar de mediante la secuencia de puertas lógicas de su implementación física. Para ello en un inicio se propone una implementación base sencilla, la cual se va refinando para optimizar su consumo de memoria y tiempo de ejecución, siendo el objetivo último el conseguir poder operar el máximo número de qubits en el menor tiempo posible.
	
	Las diferentes implementaciones realizadas (mostradas en la figura \ref{pic:general_class_diagram}) son:
	
	\begin{description}
		
		\item [\textit{base\_impl}:] Implementación de partida del algoritmo sin ninguna optimización.
		
		\item [\textit{mem\_opt\_1}:] Implementación que parte de \textit{base\_impl} y trata de optimizar el uso de memoria generando y aplicando el operador fila a fila.
		
		\item [\textit{mem\_opt\_2}:] Implementación que parte de \textit{base\_impl} y elimina la conversión a objeto de qutip del operador antes de operarlo, evitando una tener una matriz de grandes dimensiones como variable.
		
		\item [\textit{mem\_opt\_3}:] Implementación conjunta que une las optimizaciones de \textit{mem\_opt\_1} y \textit{mem\_opt\_2}.
		
		\item [\textit{time\_opt\_1}:] Implementación que partiendo de \textit{mem\_opt\_3} pre-calcula los posibles términos del operador.
		
		\item [\textit{time\_opt\_2}:] Implementación que, con \textit{mem\_opt\_3} como base, paraleliza la generación y aplicación del operador.
		
		\item [\textit{time\_opt\_3}:] Implementación que unifica \textit{time\_opt\_1} y \textit{time\_opt\_2}.
		
	\end{description}
	
	\begin{figure}[ht!]
		\includegraphics[width=\textwidth]{General_class_diagram}
		\centering
		\caption{Diagrama de clases: Implementaciones}
		\label{pic:general_class_diagram}
	\end{figure}
	
	\section{Diseño de las mediciones}
	\label{sec:measurement_design}
	
		Para cada implementación, excepto la de \textit{qutip}, se ha llevado a cabo un estudio, tanto teórico como practico, para el coste espacial y temporal. En concreto el coste espacial se ha estudiado desde el punto de vista teórico para calcular un coste mínimo y estimar una complejidad espacial y desde el punto de vista práctico para calcular los límites reales del algoritmo. En lo tocante al coste temporal, en el estudio teórico se ha mirado la complejidad ciclomática y de forma empírica tomando el tiempo interno del sistema antes y después de ejecutar el algoritmo para calcular su diferencia.
	
		Las mediciones empíricas se han realizado con 10 ejecuciones del algoritmo en las cuales se ha medido el consumo de memoria y el tiempo utilizado. Sobre el consumo de memoria se ha guardado el valor máximo (pues a la hora de saber si es ejecutable ne un sistema lo relevante es el máximo consumo) y para el tiempo se ha calculado la media y varianza (truncadas a 5 decimales). Estas mediciones se han llevado a cabo para estados de diferentes tamaños con el número de qubits (variando entre 1 y 15 pues para más de 15 el tiempo de ejecución se dispara) y se han recogido en tablas comparativas. En caso de que la memoria del sistema no fuera suficiente para ejecutar el algoritmo se muestra mediante ``x'' en las casillas de memoria y tiempo correspondientes.
	
		Para comprobar el consumo real de memoria por parte unicamente del proceso se ha usado la llamada al sistema proporcionada por la propia librería de python3.4 (\verb|resource.getrusage(resource.RUSAGE_SELF).ru_maxrss|).
		
		En cada implementación se incluye una tabla con los resultados de sus mediciones junto con una tabla comparativa de consumo de memoria y otra de tiempo de ejecución con la implementación que se usó como base en esa iteración.
		
		Ademas, al final de la etapa de optimización de memoria y de la de tiempo, se incluyen tablas comparativas de toda la etapa.
		
		Como final, en el capitulo de resultados (\ref{chap:resultados}) se exponen todos los resultados obtenidos en dos tablas comparativas (una para memoria y otra para tiempo).
	
	\section{Validación}
	
		Para la validación de las implementaciones, se realiza una prueba funcional de caja negra comparando los resultados de aplicar el operador de la transformada ofrecida por \textit{qutip} con los obtenidos durante la ejecución del algoritmo para un mismo estado de entrada aleatorio. Puesto que de una implementación a otra solo se esperan cambios no funcionales, esta prueba es válida para todas las implementaciones. Como inciso, al realizar la prueba se vio que los resultados no coincidían. Después de investigarlo, se descubrió que el problema radicaba en el redondeo, pues por ejemplo si en una posición del operador tiene que haber el valor $0+5j$ el valor que aparecía en los operadores obtenidos (tanto el del algoritmo como el de \textit{qutip}) era del estilo de $3.4 * 10^{-17} + 0.5j$, que aunque muy cercano al valor esperado, no es el mismo. Por ello, se decidió redondear al 5 decimal los valores del operador, para evitar esos problemas de redondeo.
	
	\section{Diseño de clases}
	
		En lo referente al diseño de clases, se han tomado ciertas decisiones comunes a todas las implementaciones.
		
		Durante todo el proyecto se ha limitado las entradas a estados cuánticos con un número de filas potencia de 2 asumiendo que esta operación se aplicara a estados de $n$ qubits, los cuales se representan con vectores de $2^{n}$ filas. Con esto, se puede utilizar la versión simplificada de la transformada.

		Se ha hecho que todas las implementaciones estudiadas cumplan una interfaz común con un método \verb|execute(self, v)| donde \verb|v| es el estado de entrada y el valor de retorno es el resultado de aplicar la transformada cuántica de Fourier a dicho estado.
		
		Para simplificar el estudio de las implementaciones se ha implementado una superclase con métodos destinados a este fin:
		
		\begin{itemize}
			
			\item \verb|measure(self, steps=1, min_q=1, max_q=15, verbose=0)| \\ \\
				El objetivo de este método es sacar datos sobre la ejecución del algoritmo a fin de comparar con otras implementaciones. Para ello lo ejecuta \verb|steps| número de veces para estados de entrada aleatorios con tamaño entre \verb|min_q| y \verb|max_q|, tomando mediciones de consumo de tiempo y memoria para luego exportarlas a un fichero bajo una estructura de tabla de LaTeX. En caso de que \verb|steps| sea mayor que 1, se calcula la media y varianza dle coste temporal y se guarda el máximo consumo de memoria.
				
			\item \verb|test_limits (self, min_q=1, max_q=25, verbose=0)| \\ \\
				Este método trata de encontrar empíricamente el mayor estado operable por la implementación concreta ejecutando el algoritmo para estados de tamaño entre \verb|min_q| y \verb|max_q| y mide su consumo de memoria hasta que se produzca un error de memoria, para luego volcar los consumos de memoria obtenidos en un fichero con estructura de tabla de LaTeX. 
				
				En caso de que la implementación concreta alcance el máximo consumo de memoria sin necesidad de ejecutar el algoritmo hasta le final, a fin de hacer factible la medición en un tiempo razonable y puesto que aquí solo interesa el consumo de memoria, este método llama a una implementación alternativa que ejecuta el mismo algoritmo hasta que alcanza el máximo de memoria que ha de usar, reduciendo el tiempo necesario para realizar las mediciones drásticamente. 
			
			\item \verb|test (self)| \\ \\
				Con este método se realizan las pruebas funcionales de las implementaciones para su verificación. Compara el resultado de aplicar al mismo estado aleatorio la implementación de \textit{qutip} (redondeando al 5º decimal) y la implementación a probar. Esto lo hace para estados de tamaño entre 1 y 12 (inclusives) pues la transformación de \textit{qutip} tiene 12 como límite superior.
			
			\item \verb|update_memory_spent(self)| \\ \\
				Realiza una medición de la memoria consumida por el proceso, y si es superior al máximo registrado, actualiza el máximo.
			
		\end{itemize}
	
	
	\input{development/qutip_implementation}
	
	\input{development/base_implementation}
	%\input{development/base_implementation_old}
	
	\input{development/memory_improvement}
	%\input{development/memory_improvement_old}
	
	\input{development/time_improvement}
	%\input{development/time_improvement_old}
	
