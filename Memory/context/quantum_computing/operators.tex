\subsection{Operadores}
	
		En computación las operaciones se llevan a cabo mediante el uso de puertas lógicas sobre un estado. 
		
		Las operaciones se representan como matrices y su aplicación se corresponde a la multiplicación por la izquierda de dicha matriz operador sobre la representación matricial del estado.
		
		Por ejemplo la operación NOT que actúa sobre bits individuales y conmuta el estado $\ket{0}$ por $\ket{1}$ y viceversa se representa mediante la matriz operador:
		
		\begin{tikzpicture}[circuit logic US, huge circuit symbols]
			\matrix[column sep=10mm]
			{
				\node [not gate] (not) {}; \\
			};
			
			
			\node (c) at ([xshift=-10mm]not.west) {};
			
			\draw (not.input) -- (not.input-|c.east) node[left] (a) {};
			\draw (not.output) -- ++(right:10mm);
		\end{tikzpicture}
		
		$$
			NOT = \arr{ 0 & 1 \\ 1 & 0}
		$$
		
		y se puede comprobar que
		
		$$
			\ubmat
			{
				NOT \ket{0} = \ket{1} & & NOT \ket{1} = \ket{0} \\
				 & & \\
				\arr{ 0 & 1 \\ 1 & 0} \arr{ 1 \\ 0 } = \arr{ 0 \\ 1} & & \arr{ 0 & 1 \\ 1 & 0} \arr{ 0 \\ 1 } = \arr{ 1 \\ 0}
			}
		$$
		
		Otro ejemplo puede ser la puerta lógica AND que tiene por entrada un estado compuesto por dos bits y por salida un estado de un solo bit. La salida se corresponde con el $\ket{1}$ unicamente cuando en la entrada el estado es $\ket{11}$, siendo $\ket{0}$ en cualquiera de los tres casos restantes. Matricialmente se representa como

			\begin{tikzpicture}[circuit logic US, huge circuit symbols]
				\matrix[column sep=10mm]
				{
					\node [and gate] (and) {}; \\
				};
				
				
				\node (c) at ([xshift=-10mm]and.west) {};
				
				\draw (and.input 2) -- (and.input 2-|c.east) node[left] (b) {};
				\draw (and.input 1) -- (and.input 1-|c.east) node[left] (a) {};
				\draw (and.output) -- ++(right:10mm);
			\end{tikzpicture}

			$$
				AND = \arr{ 1 & 1 & 1 & 0 \\ 0 & 0 & 0 & 1 } 
			$$

		
		y se puede comprobar que, por ejemplo
		
		$$
			\ubmat
			{
				AND \ket{11} = \ket{1} 
				& & 
				AND \ket{01} = \ket{0} \\
				& & \\
				\arr{ 1 & 1 & 1 & 0 \\ 0 & 0 & 0 & 1 } \arr{ 0 \\ 0 \\ 0 \\ 1 } = \arr{ 0 \\ 1} 
				& & 
				\arr{ 1 & 1 & 1 & 0 \\ 0 & 0 & 0 & 1 } \arr{ 0 \\ 1 \\ 0 \\ 0 } = \arr{ 1 \\ 0}
			}
		$$
		
		Generalizando, un operador que actúe sobre un estado de n bits y devuelva un estado de m bits sera una matriz con $2^{n}$ columnas y $2^{m}$ filas donde cada columna se corresponde a un posible estado de entrada, cada fila a un estado de salida, un valor de 1 indica que esa entrada esta relacionada con esa salida y uno de 0 lo contrario. 
		
		Así por ejemplo, en la matriz del operador AND tiene por entrada un estado de 2 elementos y por salida un estado de 1 elemento, por lo que es una matriz $2^{2} \times 2^{1} = 4 \times 2$. Para las columnas de los estados de entrada $\ket{00}$, $\ket{01}$ y $\ket{10}$ tenemos un 1 en la fila correspondiente a la salida $\ket{0}$ y un 0 en la correspondiente a $\ket{1}$, siendo lo contrario para la entrada correspondiente al estado $\ket{11}$.

		$$
			\ubmat
			{
				& \ubmat {\textbf{00} & \textbf{01} & \textbf{10} & \textbf{11}} \\
				\ubmat {\textbf{0} \\ \textbf{1}} & 
				\arr{\hspace{0.5em}  1 \hspace{0.5em} & 1 \hspace{0.5em} & 1 \hspace{0.5em} & 0\hspace{0.5em}  \\ 
					\hspace{0.5em} 0 \hspace{0.5em} & 0 \hspace{0.5em} & 0 \hspace{0.5em} & 1 \hspace{0.5em} }
			}
		$$

		\subsubsection{Combinación de operadores}
		
			Los operadores pueden ser combinados en serie para una aplicación secuencial sobre el mismo estado o en paralelo para aplicar diferentes operadores a diferentes qubits del estado de manera simultanea.
			
			Para combinarlos en serie se aplica una operación después de otra por orden, así si queremos realizar un AND para luego negar su resultado con un NOT, cogemos sus matrices y las multiplicamos en orden por la izquierda. 
			
			$$NOT * (AND * \ket{ab})$$
			
			Otra forma es aprovechar las propiedades del álgebra vectorial y crear una operación que efectúe ambas operaciones simultáneamente, en este caso llamada NAND, aprovechando que 
			
			$$NOT * (AND * \ket{ab}) = (NOT * AND) * \ket{ab} = NAND * \ket{ab}$$
			
			{
				\centering
				\begin{tabular} {cc}
					
					\begin{tikzpicture}[circuit logic US, huge circuit symbols]
						\matrix[column sep=10mm]
						{
							\node [and gate] (and) {AND}; & \node[not gate] (not) {NOT};\\
						};
						
						\node (c) at ([xshift=-10mm]and.west) {};
						
						\draw (and.input 2) -- (and.input 2-|c.east) node[left] (b) {b};
						\draw (and.input 1) -- (and.input 1-|c.east) node[left] (a) {a};
						\draw (not.input) -- (and.output) node[left] () {};
						\draw (not.output) -- ++(right:10mm);
					\end{tikzpicture}
					
					&
					
					\begin{tikzpicture}[circuit logic US, huge circuit symbols]
					\matrix[column sep=10mm]
					{
						\node [nand gate] (nand) {NAND};\\
					};
					
					\node (c) at ([xshift=-10mm]nand.west) {};
					
					\draw (nand.input 2) -- (nand.input 2-|c.east) node[left] (b) {b};
					\draw (nand.input 1) -- (nand.input 1-|c.east) node[left] (a) {a};
					\draw (nand.output) -- ++(right:10mm);
					\end{tikzpicture}
					
				\end{tabular}
				\\
			}
			
			$$
				NOT * AND = \arr{ 0 & 1 \\ 1 & 0} * \arr{ 1 & 1 & 1 & 0 \\ 0 & 0 & 0 & 1 } = \arr{ 0 & 0 & 0 & 1 \\ 1 & 1 & 1 & 0 } = NAND
			$$
			
			Para combinar operadores en paralelo, por ejemplo partiendo de un estado de 3 bits ($\ket{abc}$) queremos negar (NOT) el primer bit (a) y hacer un AND sobre los otros dos (b y c), se utiliza el producto tensorial de los operadores, poniendo más a la izquierda aquellos que afecten a los primeros bits. Esto es, puesto que el estado compuesto se obtendría mediante $a \otimes b \otimes c$, y puesto que $a$ es el primer elemento, $NOT$ será también el primer elemento al combinar los operadores. Por ello, nuestro operador se construiría mediante $NOT \otimes AND$.
			
			
			\begin{tikzpicture}[circuit logic US, huge circuit symbols]
				\matrix[column sep=10mm]
				{
					\node [not gate] (not) {};\\
					\node () {}; \\
					\node [and gate] (and) {};\\
				};
				
				\node (a) at ([xshift=-10mm]not.west|-not.input) {a};
				\node (b) at ([xshift=-10mm]and.west|-and.input 1) {b};
				\node (c) at ([xshift=-10mm]and.west|-and.input 2) {c};
				
				\draw (not.input) -- (a);
				\draw (and.input 1) -- (b);
				\draw (and.input 2) -- (c);
				\draw (not.output) -- ++(right:10mm);
				\draw (and.output) -- ++(right:10mm);
			\end{tikzpicture}
			
			$$
				a \otimes b \otimes c \Rightarrow NOT \otimes AND = \arr{ 0 & 1 \\ 1 & 0} \otimes \arr{ 1 & 1 & 1 & 0 \\ 0 & 0 & 0 & 1 } = 
				\arr{ 
					0 & 0 & 0 & 0 & 1 & 1 & 1 & 0 \\ 
					0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 \\
					1 & 1 & 1 & 0 & 0 & 0 & 0 & 0 \\
					0 & 0 & 0 & 1 & 0 & 0 & 0 & 0
					}
			$$
	
		\subsubsection{Reversibilidad}
		
			Ahora bien, no todas las operaciones (puertas) lógicas son aplicables en computación cuántica pues, con excepción de las operaciones de medición, para que una operación sea aplicable en computación cuántica debe ser reversibles y poder representarse por una matriz unitaria. Por ejemplo la operación AND no es reversible, pues si el resultado es $\ket{1}$ sabemos que la entrada era $\ket{11}$ pero si el resultado es $\ket{0}$ no hay forma de saber, solo con esa información, si la entrada era $\ket{00}$, $\ket{01}$ o $\ket{10}$. En contraste, la operación NOT si es que reversible, pues si el resultado es $\ket{1}$ sabemos que la entrada fue necesariamente $\ket{0}$ y viceversa. En concreto, la operación NOT es su propia inversa, siendo 
			
			$$NOT * NOT = I_{2}$$
			
			%https://en.wikipedia.org/wiki/Landauer%27s_principle
			En 1961 Rolf Landauer, tras analizar los procesos computacionales, postuló que era el proceso de pérdida de información (y no su escritura) lo que provocaba la pérdida de energía y por tanto generación de calor. Esto ha sido comprobado (y publicado \cite{PhysRevLett.113.190601}) recientemente, confirmando dicha teoría.
			
			Siguiendo esa linea de pensamiento, en 1972 Charles H. Bennett llego a la conclusión de que, siendo el borrado de información es lo que generaba la pérdida de energía, si un ordenador fuera reversible y no eliminase información, no debiera tener consumo energético ni disipar calor. Partiendo de esa idea, empezó a trabajar en circuitos y programas reversibles.
			
			Una de las puertas reversibles más importantes es la puerta de Toffoli
			
			$$	
				\Qcircuit @C=2em @R=2em @! {
					\lstick{\ket{x}} & \ctrl{2} & \rstick{\ket{x}} \qw \\
					\lstick{\ket{y}} & \ctrl{1} & \rstick{\ket{y}} \qw \\
					\lstick{\ket{z}} & \targ & \rstick{\ket{z \oplus (x \wedge y)}} \qw \\
				}
			$$
			
			Esta puerta tiene dos lineas de control ($\ket{x}$ y $\ket{y}$) y una linea de información ($\ket{z}$). En caso de que las dos lineas de control se encuentren activadas ($\ket{1}$), la linea de información en negada. 
			
			Esta operación es un mapeado $\ket{x, y, z} \mapsto \ket{x, y, z \oplus (x \wedge y)}$ y está representada por la matriz
			
			$$
				\ubmat{
					& 
					\ubmat{\textbf{000} & \textbf{001} & \textbf{010} & \textbf{011} & \textbf{100} & \textbf{101} & \textbf{110} & \textbf{111}} \\
					\ubmat{\textbf{000} \\ \textbf{001} \\ \textbf{010} \\ \textbf{011} \\ \textbf{100} \\ \textbf{101} \\ \textbf{110} \\ \textbf{111}} &
					\begin{bmatrix}
						\hspace{0.6em} 1 \hspace{0.6em} & \hspace{0.6em} 0 \hspace{0.6em} & \hspace{0.6em} 0 \hspace{0.6em} & \hspace{0.6em} 0 \hspace{0.6em} & \hspace{0.6em} 0 \hspace{0.6em} & \hspace{0.6em} 0 \hspace{0.6em} & \hspace{0.6em} 0 \hspace{0.6em} & \hspace{0.6em} 0 \hspace{0.6em}\\
						0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 \\
						0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 \\
						0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 \\
						0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 \\
						0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 \\
						0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 \\
						0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 \\
					\end{bmatrix}
				}
			$$
			
			La principal importancia de la puerta de Toffoli es que es universal. Esto significa que cualquier otra puerta lógica puede construirse a base de puertas de Toffoli. Concretamente, solo usando puertas de Toffoli se podría construir un ordenador reversible que, en teoría, no tendría consumo energético ni dispersaría calor.
			
			Para demostrar dicha condición de puerta universal, una forma sencilla es reproducir el comportamiento de otro conjunto de puertas universales ya demostradas. En este caso, las puertas AND y NOT.
			
			Partiendo de una puerta de Toffoli, si ponemos las entradas $\ket{x}$ y $\ket{y}$ constantes con $\ket{1}$, obtendremos una operación que siempre negara el contenido la entrada $\ket{z}$, lo que es lo mismo que hacer $NOT * \ket{z}$. 
			
			Por otro lado, si lo que ponemos constante es la entrada $\ket{z}$ con el valor $\ket{0}$, obtenemos en dicha linea el resultado de $\ket{x \wedge y}$. 
			
			Finalmente, para poder construir todas las puertas es importante también poder duplicar un valor concreto. Mediante una puerta de Toffoli con $\ket{x} = \ket{1}$ y $\ket{z} = \ket{0}$, a la salida de la linea de $\ket{z}$ obtenemos una copia de $\ket{y}$.
			
			$$	
				\ubmat{
					\textbf{NOT} & & \textbf{AND} & & \textbf{COPY} \\
					\Qcircuit @C=2em @R=2em @! {
						\lstick{\ket{1}} & \ctrl{2} & \rstick{\ket{1}} \qw \\
						\lstick{\ket{1}} & \ctrl{1} & \rstick{\ket{1}} \qw \\
						\lstick{\ket{z}} & \targ & \rstick{\ket{\neg z}} \qw \\
					}
					& \hspace{6.5em} &
					\Qcircuit @C=2em @R=2em @! {
						\lstick{\ket{x}} & \ctrl{2} & \rstick{\ket{x}} \qw \\
						\lstick{\ket{y}} & \ctrl{1} & \rstick{\ket{y}} \qw \\
						\lstick{\ket{0}} & \targ & \rstick{\ket{x \wedge y}} \qw \\
					}
					& \hspace{4.5em} &
					\Qcircuit @C=2em @R=2em @! {
						\lstick{\ket{1}} & \ctrl{2} & \rstick{\ket{1}} \qw \\
						\lstick{\ket{y}} & \ctrl{1} & \rstick{\ket{y}} \qw \\
						\lstick{\ket{0}} & \targ & \rstick{\ket{y}} \qw \\
					}
				}
			$$
			
			Otra puerta reversible universal es la puerta de Fredkin, también con 3 entradas $\ket{x, y, z}$ y tres salidas $\ket{x, y', z'}$.
			
			$$	
				\Qcircuit @C=2em @R=2em @! {
					\lstick{\ket{x}} & \ctrl{1} \qw & \rstick{\ket{x}} \qw \\
					\lstick{\ket{y}} & \qswap \qw & \rstick{\ket{y'}} \qw \\
					\lstick{\ket{z}} & \qswap \qwx & \rstick{\ket{z'}} \qw \\
				}
			$$

			
			La salida $\ket{x}$ siempre es igual a su entrada, mientras que las salidas $\ket{y', z'}$ dependen del valor de $\ket{x}$. Si es $\ket{0}$ se mantiene la salida de la entrada original, y si es $\ket{1}$ se intercambien las entradas. Así, hay dos opciones $\ket{0, y, z} \mapsto \ket{0, y, z}$ y $\ket{1, y, z} \mapsto \ket{1, z, y}$.
			
			La matriz correspondiente de este operador es
			
			$$
				\ubmat{
					& 
					\ubmat{\textbf{000} & \textbf{001} & \textbf{010} & \textbf{011} & \textbf{100} & \textbf{101} & \textbf{110} & \textbf{111}} \\
					\ubmat{\textbf{000} \\ \textbf{001} \\ \textbf{010} \\ \textbf{011} \\ \textbf{100} \\ \textbf{101} \\ \textbf{110} \\ \textbf{111}} &
					\begin{bmatrix}
					\hspace{0.6em} 1 \hspace{0.6em} & \hspace{0.6em} 0 \hspace{0.6em} & \hspace{0.6em} 0 \hspace{0.6em} & \hspace{0.6em} 0 \hspace{0.6em} & \hspace{0.6em} 0 \hspace{0.6em} & \hspace{0.6em} 0 \hspace{0.6em} & \hspace{0.6em} 0 \hspace{0.6em} & \hspace{0.6em} 0 \hspace{0.6em}\\
					0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 \\
					0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 \\
					0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 \\
					0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 \\
					0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 \\
					0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 \\
					0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 \\
					\end{bmatrix}
				}
			$$
			
			La puerta AND se realizar poniendo $\ket{y} = \ket{0}$ y la puerta NOT  poniendo $\ket{y} = \ket{1}$ y $\ket{z} = \ket{0}$.
			
			$$	
				\ubmat{
					\textbf{NOT} & & \textbf{AND} \\
					\Qcircuit @C=2em @R=2em @! {
						\lstick{\ket{x}} & \ctrl{1} \qw & \rstick{\ket{x}} \qw \\
						\lstick{\ket{1}} & \qswap \qw & \rstick{\ket{\neg x}} \qw \\
						\lstick{\ket{0}} & \qswap \qwx & \rstick{\ket{x}} \qw \\
					}
					& \hspace{7em} &
					\Qcircuit @C=2em @R=2em @! {
						\lstick{\ket{x}} & \ctrl{1} \qw & \rstick{\ket{x}} \qw \\
						\lstick{\ket{0}} & \qswap \qw & \rstick{\ket{x \wedge z}} \qw \\
						\lstick{\ket{z}} & \qswap \qwx & \rstick{\ket{(\neg x) \wedge z}} \qw \\
					}
				}
			$$

			
			
		\subsubsection{Puertas cuánticas}
		
		\subsubsection{Hadamard}
		
		\subsubsection{Desplazamiento de fase}