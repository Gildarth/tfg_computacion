\subsubsection{Transformada de Fourier Discreta}

	La transformada de Fourier discreta es una transformación de una función matemática a otra, obteniendo una representación en el dominio de la frecuencia. Un requisito es que la función de entrada sea una secuencia discreta, de duración finita y compuesta de números reales o complejos.
	
	Así, partiendo de un vector de longitud $N$ con la imagen de la función a transformar $X$ obtenemos el vector correspondiente en el dominio de la frecuencia $Y$ mediante la transformación
	
	$$
		\ubmat{
			X = [x_{0}, x_{2}, \dots, x_{N-1}]
			& \mapsto &	
			Y = [y_{0}, y_{2}, \dots, y_{N-1}]
		}
	$$
	
	tal que
	
	$$
		y_{k} = \frac{1}{\sqrt{N}} \sum_{j=0}^{N-1}x_{j}e^{\frac{2 \pi i}{N}jk}
	$$
	
	Por comodidad, se suele representar agrupando varios términos constantes simplificando así su representación:
	
	$$
		\ubmat{
			w = e^{\frac{2 \pi i}{N}}
			& \Rightarrow &	
			y_{k} = \frac{1}{\sqrt{N}} \sum_{j=0}^{N-1}x_{j}w^{jk}
		}
	$$
	
	Esta operación es reversible, siendo su inversa
	
	$$
		x_{j} = \frac{1}{\sqrt{N}} \sum_{k=0}^{N-1}y_{k}w^{-jk}
	$$
	

\subsubsection{Transformada cuántica de Fourier}

	Puesto que la transformada discreta actúa sobre un vector discreto y finito de números reales o complejos, y ya que un estado cuántico se representa mediante un vector de números complejos, esta transformación debiera poderse aplicar a un estado cuántico siempre que se pueda implementar en un circuito cuántico.
	
	Así, tenemos una transformación desde un estado de n qubits $\ketp{}$ representado por un vector de $N = 2^{n}$ números complejos  , a otro $F\ketp{}$ que se corresponde con el primero, pero en el dominio de la frecuencia
	
	$$
		\ubmat{
			\ketp{} = \sum_{\substack{j=0}}^{N-1} a_{j} \ket{j}
			& \mapsto &	
			F\ketp{} = \sum_{\substack{k=0}}^{N-1} b_{k} \ket{k}
		}
	$$
	
	siendo (de forma análoga a la transformada discreta de Fourier)
	
	$$
		\ubmat{
			b_{k} = \frac{1}{\sqrt{N}} \sum_{j=0}^{N-1}a_{j}w^{jk}
			& & \wedge & &
			w = e^{\frac{2 \pi i}{N}}
		}
	$$
	
	El término $\frac{1}{\sqrt{N}}$ sirve para normalizar el vector, pues un estado cuántico ha de ser un vector normalizado.
	
	Por ejemplo, para el caso de un estado de 2 qubits (N = 4)
	
	$$
		\ubmat{
			\ketp{} = [a_{0}, a_{1}, a_{2}, a_{3}]^{T} = a_{0} \ket{00} + a_{1} \ket{01} + a_{2} \ket{10} + a_{3} \ket{11}
			\\ \\
			F\ketp{} = [b_{0}, b_{1}, b_{2}, b_{3}]^{T} = b_{0} \ket{00} + b_{1} \ket{01} + b_{2} \ket{10} + b_{3} \ket{11}
		}
	$$
	
	calculando los valores del estado $F\ketp{}$ por separado
	
	$$
		\ubmat{
			b_{0} = \frac{1}{\sqrt{4}} \sum_{j=0}^{4-1}a_{j}w^{0j} = \frac{1}{2} \sum_{j=0}^{3}a_{j}w =
				\frac{1}{2} [a_{0}w^{0} + a_{1}w^{0} + a_{2}w^{0} + a_{3}w^{0}]
			\\ \\
			b_{1} = \frac{1}{\sqrt{4}} \sum_{j=0}^{4-1}a_{j}w^{1j} = \frac{1}{2} \sum_{j=0}^{3}a_{j}w^{j} =
				\frac{1}{2} [a_{0}w^{0} + a_{1}w^{1} + a_{2}w^{2} + a_{3}w^{3}]
			\\ \\
			b_{2} = \frac{1}{\sqrt{4}} \sum_{j=0}^{4-1}a_{j}w^{2j} = \frac{1}{2} \sum_{j=0}^{3}a_{j}w^{2j} =
				\frac{1}{2} [a_{0}w^{0} + a_{1}w^{2} + a_{2}w^{4} + a_{3}w^{6}]
			\\ \\
			b_{3} = \frac{1}{\sqrt{4}} \sum_{j=0}^{4-1}a_{j}w^{3j} = \frac{1}{2} \sum_{j=0}^{3}a_{j}w^{3j} =
				\frac{1}{2} [a_{0}w^{0} + a_{1}w^{3} + a_{2}w^{6} + a_{3}w^{9}]
		}
	$$
	
	es posible realizar esta operación mediante matrices, así tenemos una matriz operador $F_{4}$ que se aplica al vector que representa el estado $\ketp{}$.
	
	$$
		F_{4} = \arr{
			w^{0} & w^{0} & w^{0} & w^{0}\\ 
			w^{0} & w^{1} & w^{2} & w^{3}\\ 
			w^{0} & w^{2} & w^{4} & w^{6}\\ 
			w^{0} & w^{3} & w^{6} & w^{9}\\ 
		} = \arr{
			1 & 1 & 1 & 1 \\ 
			1 & w^{1} & w^{2} & w^{3}\\ 
			1 & w^{2} & w^{4} & w^{6}\\ 
			1 & w^{3} & w^{6} & w^{9}\\ 
		}
	$$
	
	y gracias a que $w = e^{\frac{2 \pi i}{4}} = e^{\frac{\pi i}{2}}$, tenemos que para el caso de 2 qubits $w^{4} = e^{2 \pi} = 1$ y por lo tanto, por ejemplo, $w^{6} = w^{4}w^{2} = 1w^{2} = w^{2}$, lo que nos permite simplificar la matriz operador a
	
	$$
		F_{4} = \arr{
			1 & 1 & 1 & 1 \\ 
			1 & w^{1} & w^{2} & w^{3}\\ 
			1 & w^{2} & 1 & w^{2}\\ 
			1 & w^{3} & w^{2} & w^{1}\\ 
		}
	$$
	
	Por lo tanto
	
	$$
		F_{4}\ketp{} = 
		\arr{
			1 & 1 & 1 & 1 \\ 
			1 & w^{1} & w^{2} & w^{3}\\ 
			1 & w^{2} & 1 & w^{2}\\ 
			1 & w^{3} & w^{2} & w^{1}\\ 
		} 
		\arr{a_{0} \\ a_{1} \\ a_{2} \\ a_{3}} = 
		\arr{b_{0} \\ b_{1} \\ b_{2} \\ b_{3}}
	$$
	
	Esta matriz operador es generalizable para cualquier N
	
	$$
		F_{N} = \arr{
			w^{0} & w^{0} & w^{0} & w^{0} & \cdots & w^{0} \\ 
			w^{0} & w^{1} & w^{2} & w^{3} & \cdots & w^{N-1} \\ 
			w^{0} & w^{2} & w^{4} & w^{6} & \cdots & w^{2(N-1)} \\ 
			w^{0} & w^{3} & w^{6} & w^{9} & \cdots & w^{3(N-1)} \\ 
			\vdots  & \vdots & \vdots & \vdots  & \ddots & \vdots \\ 
			w^{0} & w^{N-1} & w^{2(N-1)} & w^{3(N-1)} & \cdots & w^{(N-1)(N-1)}
		}
	$$
	
	y ademas se puede comprobar que es unitaria (y por lo tanto reversible siendo su adjunta su propia inversa) pues 
	
	$$
		FF^{\dagger} = I = F^{\dagger}F
	$$
	
	
	
	\todo{Para mañana, implementación en un circuito cuántico}
	
	
	
	
	
	
	
	
	
	