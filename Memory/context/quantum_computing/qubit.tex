\subsection{Unidad de información: El qubit}

	El qubit es la unidad de información básica en computación cuántica (de la misma forma que el bit lo es en computación clásica). 
	
	Para representar un qubit se utiliza la llamada notación de Dirac (también conocida como notación bra-ket) en la cual un estado se representa mediante una matriz columna o ket ($\ket{\hspace{0.5em}}$). Así, tenemos dos estados básicos
	$$
		\ket{0} = \ubmat{ \textbf{0} \\ \textbf{1} } \arr{ 1 \\ 0 }
		\hspace{3cm}
		\ket{1} = \ubmat{ \textbf{0} \\ \textbf{1} } \arr{ 0 \\ 1 }
	$$
	(correspondientes a los estados binarios de un bit) y la superposición cuántica de ambos estados se representa por su combinación lineal
	$$
		c_{0} \cdot \ket{0} + c_{1} \cdot \ket{1} =
		c_{0} \cdot \arr{ 1 \\ 0 } + c_{1} \cdot \arr{ 0 \\ 1 } = 
		\arr{ c_{0} \\ c_{1} }
	$$
	tal que $c_{0}$ y $c_{1}$ son números complejos y $\abs{c_{0}}^{2} + \abs{c_{1}}^{2} = 1$ (probabilidad total), siendo $\abs{c_{0}}^{2}$ la probabilidad de que, al medir el qubit, este se encuentre en el estado básico $\ket{0}$ y $\abs{c_{1}}^{2}$ el análogo para el estado básico $\ket{1}$. Por ello un qubit cualquiera se representa como un ket o matriz columna perteneciente a 
	$$
		C^{2} = \ubmat{ \textbf{0} \\ \textbf{1} } \arr{ c_{0} \\ c_{1}}
	$$
	
	Un ejemplo de qubit con igual probabilidad de colapsar al estado $c_{0}$ o al estado $c_{1}$ es representado como
	$$
		\arr{\frac{1}{\sqrt{2}} \\ \frac{1}{\sqrt{2}}} = 
		\frac{1}{\sqrt{2}} \arr{ 1 \\ 1 } = 
		\frac{1}{\sqrt{2}} \ket{0} + \frac{1}{\sqrt{2}} \ket{1} =
		\frac{\ket{0} + \ket{1}}{\sqrt{2}} =
		\frac{\ket{1} + \ket{0}}{\sqrt{2}}
	$$
	
	Como detalle a destacar
	$$
		\arr{\frac{1}{\sqrt{2}} \\ \frac{-1}{\sqrt{2}}} =
		\frac{\ket{0} - \ket{1}}{\sqrt{2}} \neq
		\frac{\ket{1} - \ket{0}}{\sqrt{2}} = 
		\arr{\frac{-1}{\sqrt{2}} \\ \frac{1}{\sqrt{2}}}
	$$
	
	Llegados a este punto ya se ha descrito como representar un qubit, pero lo interesante es poder representar un sistema con varios qubits. Esto se hace de forma análoga a la representación de sistemas de varios bits, donde se agrupan un número n de bit a fin de poder representar $2^{n} -1$ estados diferentes frente a los 2 estados representables con un solo bit. Un ejemplo de un estado posible para un byte (8 bits) es
	$$
		11010010 = 
		\ket{1}\ket{1}\ket{0}\ket{1}\ket{0}\ket{0}\ket{1}\ket{0} = 
		\arr{ 0 \\ 1 }, \arr{ 0 \\ 1 }, \arr{ 1 \\ 0 }, \arr{ 0 \\ 1 }, \arr{ 1 \\ 0 }, \arr{ 1 \\ 0 }, \arr{ 0 \\ 1 }, \arr{ 1 \\ 0 }
	$$
	
	De forma análoga se combinan n qubits para formar un sistema capaz de representar más estados. Así, para el ejemplo de 8 qubits tenemos
	$$
		C^{2} \otimes C^{2} \otimes C^{2} \otimes C^{2} \otimes C^{2} \otimes C^{2} \otimes C^{2} \otimes C^{2} = 
		(C^{2})^{\otimes 8} =
		C^{256}
	$$
	
	$$
		\ubmat
		{
			\ubmat
			{ 
				\textbf{00000000} \\ 
				\textbf{00000001} \\
				\vdots \\
				\textbf{11010011} \\
				\textbf{11010100} \\
				\vdots \\
				\textbf{11111110} \\
				\textbf{11111111} 
			}
			&
			\arr
			{ 
				c_{0} \\ 
				c_{1} \\
				\vdots \\
				c_{211} \\
				c_{212} \\
				\vdots \\
				c_{254} \\
				c_{255} 
			}
		}
	$$
	
	tal que $\sum_{\substack{i=0}}^{255} \abs{c_{i}}^{2} = 1$.
	
	Para representar un byte solo es necesario representar 8 dígitos binarios, pero para representar un qubyte (8 qubits) es necesario representar $2^{8} = 256$ números complejos. Este incremento exponencial de la capacidad necesaria es uno de los motivos por los que es inviable hacer experimentos con sistemas de muchos qubits (un estado en un sistema de 64 qubits necesita representar $2^{64} = 18.446.744.073.709.551.616$ números complejos).
	
	Así como un mismo estado en un sistema de un qubit tiene varias representaciones, lo mismo pasa con un estado en un sistema de más de un qubit 
	$$
		\ubmat
		{
			\ubmat { \textbf{00} \\ \textbf{01} \\ \textbf{10} \\ \textbf{11} }
			&
			\arr { c_{0} \\ c_{1} \\ c_{2} \\ c_{3} }
		}
		\longrightarrow
		\frac{1}{\sqrt{3}}\arr { 1 \\ 0 \\ -1 \\ 1 } =
		\frac{1}{\sqrt{3}}\ket{00} - \frac{1}{\sqrt{3}}\ket{10} + \frac{1}{\sqrt{3}}\ket{11} =
		\frac{\ket{00} - \ket{10} + \ket{11}}{\sqrt{3}}
	$$
	y en general, un estado cuántico para un sistema de dos qubits se escribe
	$$
		\ketp{} = c_{0,0}\ket{00} + c_{0,1}\ket{01} + c_{1,0}\ket{10} + c_{1,1}\ket{11}
	$$
	
	Un detalle a tener en cuenta es que la combinación de dos estados cuánticos, al igual que con los bits, no es conmutativa
	$$
		\ket{0} \otimes \ket{1} = \ket{0 \otimes 1} = \ket{01} \neq \ket{10} = \ket{1 \otimes 0} = \ket{1} \otimes \ket{0}
	$$
	
	
\subsection{Esfera de Bloch}
	
	La esfera de Bloch es una representación geométrica del espacio de estados de un sistema cuántico de un qubit. Dicha representación puede observarse en la figura \ref{pic:bloch_sphere}.
	
	Cualquier punto de la esfera de Bloch representa un estado cuántico o qubit, el cual puede expresarse como
	$$
		\ketp{} = \cos(\theta/2) \ket{0} + e^{i \phi} \sin(\theta /2) \ket{1}
	$$
	donde $\theta$ y $\phi$ son números reales tales que $0 \leq \theta \leq \pi$ y $0 \leq \phi \leq 2 \pi$ y se corresponden con el ángulo que forma el radio del punto que representa el estado cuántico con el eje Z ($\theta$) y con el eje X ($\phi$).
	
	\begin{figure}[ht!]
		\includegraphics[width=20em]{Bloch_sphere}
		\centering
		\caption{Representación de un qubit $\ketp{}$ en una esfera de Bloch}
		\label{pic:bloch_sphere}
	\end{figure}
	

