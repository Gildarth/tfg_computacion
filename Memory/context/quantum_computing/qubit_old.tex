\subsection{El qubit}
		
		En un sistema de computación clásico la unidad de información es el bit y puede adoptar en un momento dado uno (y solo uno) de dos posibles estados, $\ket{0}$ o $\ket{1}$. Esto forma un sistema bi-dimensional que se puede representar de forma matricial en un vector columna de la siguiente manera:
		

		$$
			\ket{0} = \ubmat{ \textbf{0} \\ \textbf{1} } \arr{ 1 \\ 0 }
		\hspace{3cm}
			\ket{1} = \ubmat{ \textbf{0} \\ \textbf{1} } \arr{ 0 \\ 1 }
		$$
		
		De forma similar, en computación cuántica la unidad de memoria es el qubit y tiene los mismos estados básicos, $\ket{0}$ o $\ket{1}$, pero a diferencia de el bit, que solo puede estar en uno u otro en un momento dado, un qubit puede estar en ambos estados simultáneamente, llamándose a este efecto superposición de estados. Este estado superpuesto se mantiene mientras no se realice una medición del qubit, momento en el cual este colapsa a uno de los dos estados básicos.
		
		Representaremos un qubit como una matriz perteneciente a $C^{2} = \ubmat{ \textbf{0} \\ \textbf{1} } \arr{ c_{0} \\ c_{1} }$ tal que $\abs{c_{0}}^{2} + \abs{c_{1}}^{2} = 1$, siendo $\abs{c_{0}}^{2}$ la probabilidad de que al colapsar el qubit este se encuentre en el estado $\ket{0}$ y $\abs{c_{1}}^{2}$ el análogo para el estado $\ket{1}$.
		
		Ademas, al ser las matrices asociadas a $\ket{0}$ y $\ket{1}$ las bases canónicas de $C^{2}$, todo qubit puede representarse como una combinación lineal de dichos estados
		
		$$
			\arr{ c_{0} \\ c_{1} } = 
			c_{0} \cdot \arr{ 1 \\ 0 } + c_{1} \cdot \arr{ 0 \\ 1 } = 
			c_{0} \cdot \ket{0} + c_{1} \cdot \ket{1} 
		$$ 
		
		Lo cual da lugar a diferentes representaciones para un mismo estado, por ejemplo el de superposición con la misma probabilidad de colapsar a $\ket{0}$ o a $\ket{1}$, puede representarse como:
		
		$$
			\arr{\frac{1}{\sqrt{2}} \\ \frac{1}{\sqrt{2}}} = 
			\frac{1}{\sqrt{2}} \arr{ 1 \\ 1 } = 
			\frac{1}{\sqrt{2}} \ket{0} + \frac{1}{\sqrt{2}} \ket{1} =
			\frac{\ket{0} + \ket{1}}{\sqrt{2}} =
			\frac{\ket{1} + \ket{0}}{\sqrt{2}}
		$$
		
		Otro detalle a destacar es que 
		
		$$
			\arr{\frac{1}{\sqrt{2}} \\ \frac{-1}{\sqrt{2}}} =
			\frac{\ket{0} - \ket{1}}{\sqrt{2}} \neq
			\frac{\ket{1} - \ket{0}}{\sqrt{2}} = 
			\arr{\frac{-1}{\sqrt{2}} \\ \frac{1}{\sqrt{2}}}
		$$
		
		Llegados a este punto ya se ha descrito como representar un qubit, pero lo interesante es poder representar un sistema con varios qubits. Esto se hace de forma análoga a la representación de sistemas de varios bits, donde se agrupan un número n de bit a fin de poder representar $2^{n} -1$ estados diferentes frente a los 2 estados representables con un solo bit. Un ejemplo de un estado posible para un byte (8 bits) es:
		
		$$
			11010010 = 
			\ket{1}\ket{1}\ket{0}\ket{1}\ket{0}\ket{0}\ket{1}\ket{0} = 
			\arr{ 0 \\ 1 }, \arr{ 0 \\ 1 }, \arr{ 1 \\ 0 }, \arr{ 0 \\ 1 }, \arr{ 1 \\ 0 }, \arr{ 1 \\ 0 }, \arr{ 0 \\ 1 }, \arr{ 1 \\ 0 }
		$$
		
		De forma análoga combinamos n qubits para formar un sistema capaz de representar más estados. Así, para el ejemplo de 8 qubits tenemos:
		
		$$
			C^{2} \otimes C^{2} \otimes C^{2} \otimes C^{2} \otimes C^{2} \otimes C^{2} \otimes C^{2} \otimes C^{2} = 
			(C^{2})^{\otimes 8} =
			C^{256}
		$$
		
		$$
			\ubmat
			{
				 \ubmat
				 { 
				 	\textbf{00000000} \\ 
				 	\textbf{00000001} \\
				 	\vdots \\
				 	\textbf{11010011} \\
				 	\textbf{11010100} \\
				 	\vdots \\
				 	\textbf{11111110} \\
				 	\textbf{11111111} 
				 }
				 &
				 \arr
				 { 
				 	c_{0} \\ 
				 	c_{1} \\
				 	\vdots \\
				 	c_{211} \\
				 	c_{212} \\
				 	\vdots \\
				 	c_{254} \\
				 	c_{255} 
				 }
			}
		$$
		
		tal que $\sum_{\substack{i=0}}^{255} \abs{c_{i}}^{2} = 1$.
		
		Para representar un byte solo es necesario representar 8 dígitos binarios, pero para representar un qubyte (8 qubits) es necesario representar 256 números complejos. Este incremento exponencial de la capacidad necesaria es uno de los motivos por los que es inviable hacer experimentos con sistemas de muchos qubits (un estado en un sistema de 64 qubits necesita representar $2^{64} = 18.446.744.073.709.551.616$ números complejos).
		
		Así como un mismo estado en un sistema de un qubit tiene varias representaciones, lo mismo pasa con un estado en un sistema de mas de un qubit, así 
		
		$$
			\ubmat
			{
				\ubmat { \textbf{00} \\ \textbf{01} \\ \textbf{10} \\ \textbf{11} }
				&
				\arr { c_{0} \\ c_{1} \\ c_{2} \\ c_{3} }
			}
			\longrightarrow
			\frac{1}{\sqrt{3}}\arr { 1 \\ 0 \\ -1 \\ 1 } =
			\frac{1}{\sqrt{3}}\ket{00} - \frac{1}{\sqrt{3}}\ket{10} + \frac{1}{\sqrt{3}}\ket{11} =
			\frac{\ket{00} - \ket{10} + \ket{11}}{\sqrt{3}}
		$$
		
		Y en general, un estado cuántico para un sistema de dos qubits se escribe
		
		$$
			\ketp{} = c_{0,0}\ket{00} + c_{0,1}\ket{01} + c_{1,0}\ket{10} + c_{1,1}\ket{11}
		$$
		
		Un detalle a tener en cuenta es que el producto tensorial de dos estados no es conmutativo
		
		$$
			\ket{0} \otimes \ket{1} = \ket{0 \otimes 1} = \ket{01} \neq \ket{10} = \ket{1 \otimes 0} = \ket{1} \otimes \ket{0}
		$$
	
		\subsubsection{Superposición de estados}
		
			La superposición de estados es la propiedad que permite a un elemento cuántico encontrarse, mientras que no sea medido, en más de un estado simultáneamente.
			
			Se parte de un qubit y se le aplica un operador (puerta) de Hadamard, el cual coloca el qubit de entrada en un estado de superposición equidistante de sus dos estados. Si esto se realiza sobre un conjunto de qubits, el resultado es una superposición equidistante de cada uno de los posibles estados del conjunto.
		
		\subsubsection{Colapsar a un estado (medición)}
		
			Mientras se esta trabajando con los qubits, estos se encuentran en un estado de superposición. Lo que se varía durante la ejecución del algoritmo es la probabilidad de que, al realizar la medición del sistema, el resultado sea uno u otro estado concreto de entre todos los posibles. Esto es así pues una vez se realice la medición los qubits medidos dejarán de estar en un estado de superposición para ``colapsar'' a $\ket{0}$ o $\ket{1}$ con una probabilidad $\abs{c_{i}}^{2}$ de que colapse en el estado i.
		
		\subsubsection{Entrelazamiento}
		
			El entrelazamiento es la propiedad cuántica por la cual cuando dos estados están entrelazados, lo que le ocurra a uno afectará al otro.
			
			Por ejemplo, en un sistema con dos qubits entrelazados nos encontramos con el estado
			
			$$
				\frac{\ket{11} + \ket{00}}{\sqrt{2}} = 
				\frac{1}{\sqrt{2}} \ket{11} + \frac{1}{\sqrt{2}} \ket{00}
			$$
			
			Dicho de otra manera, al colapsar dicho estado, obtendremos $\ket{11}$ o $\ket{00}$, pero nunca obtendremos $\ket{01}$ ni $\ket{10}$. Por lo tanto, si en ese sistema colapsamos solo uno de los qubits y este resulta en un estado $\ket{1}$, automáticamente sabemos que el otro qubit también se encuentra en $\ket{1}$ sin necesidad de colapsarlo, debido a que el sistema en su conjunto no permite otras opciones.
		
		