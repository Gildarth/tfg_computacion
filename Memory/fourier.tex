\chapter{Transformada de Fourier}
\minitoc
\label{chap:ourier}


%\section{Transformada de Fourier}

	La transformada de Fourier, llamada así por Jean-Baptiste Joseph Fourier, es una transformación matemática que descompone una función (la cual para unificar nomenclatura se dice que esta en el dominio del tiempo) al conjunto de frecuencias de las que se compone. La transformada de Fourier de una función se refiere tanto a la representación en el dominio de la frecuencia de la función como a la operación matemática que da lugar a dicha representación. 
	
	Una de sus propiedad más importantes que es una transformación matemática reversible, pudiendo recuperarse la función original de antes de la transformación.
	
	Esta transformación es útil para el tratamiento de datos pues las operaciones realizables en un dominio tiene su correspondiente operación equivalente en el dominio de la frecuencia, siendo a veces una mucho mas sencilla que la otra (por ejemplo, la convolución en el dominio del tiempo se corresponde con multiplicar en el de la frecuencia). Por ejemplo, si sabemos que un canal introduce un ligero ruido en una señal y se quiere eliminar, para hacerlo en el dominio del tiempo habría que suavizar la función a lo largo de todo el dominio, pero en el dominio de la frecuencia es tan sencillo como eliminar los valores asociados a las frecuencias que estén por encima de un umbral.
	
	\section{Series de Fourier}
	
		La transformada de Fourier surge de las series de Fourier, llamadas así también debido a Jean-Baptiste Joseph Fourier, el cual fue el matemático que las desarrolló y publicó \cite{fourier}.
		
		Una serie de Fourier es una forma de aproximar una función periódica como la suma de funciones oscilatorias simples (en la figura \ref{pic:fourier_series} se muestra la aproximación de un pulso cuadrado). 
		
		\begin{figure}[ht!]
			\includegraphics[width=10em]{Fourier_series}
			\centering
			\caption{Aproximación por serie de Fourier}
			\label{pic:fourier_series}
		\end{figure}
		
		Más formalmente, una serie de Fourier es un serie infinita que converge puntualmente a una función periódica y continua a trozos (o por partes) con la forma
		$$
			f(t) \sim \frac{a_{0}}{2} + \sum_{n=1}^{\infty} [
				a_{n} \cos (2n\pi f_{0} t) +
				b_{n} \sin (2n\pi f_{0} t)
			]
		$$
		donde $f_{0} = \frac{1}{T}$ es la frecuencia de la función $f$ y $a_{n}$ y $b_{n}$ son los llamados coeficientes de Fourier
		$$
			\ubmat{
				a_{n} = \frac{2}{T} \int_{-T/2}^{T/2} f(t) \cos (\frac{2n \pi}{T}t) dt
				& \hspace{2em} &
				b_{n} = \frac{2}{T} \int_{-T/2}^{T/2} f(t) \sin (\frac{2n \pi}{T}t) dt
			}
		$$
		
		Aplicando la identidad de Euler ($e^{i \pi} = \cos \pi + i \sin \pi$) se puede simplificar de forma que
		$$
			f(t) \sim \sum_{n=-\infty}^{\infty}
						c_{n}e^{2 n \pi i f_{0} t}
		$$
		$$
			c_{n} = f_{0} \int_{-T/2}^{T/2} f(t) e^{-2 n \pi i f_{0} t} dt
		$$
		
		
	\section{Transformada de Fourier continua (FT)}
	%\subsection{Transformada de Fourier continua (FT)}
		
		A partir de la serie de Fourier se define la transformada de Fourier continua como una integral de exponenciales complejas tal que
		$$
			F(\xi) = \int_{-\infty}^{+\infty} f(x) e^{-2 \pi x \xi} dx, \forall \xi \in \mathbb{R}
		$$
		donde $x$ es la variable independiente en el dominio del tiempo, $\xi$ la variable independiente en el dominio de la frecuencia, $f$ es la función en el dominio del tiempo y $F$ la función en el dominio de la frecuencia.
		
		Esta transformación es invertible, siendo la transformada inversa
		$$
			f(x) = \int_{-\infty}^{+\infty} F(\xi) e^{-2 \pi \xi x} d\xi, \forall x \in \mathbb{R}
		$$
	
	\section{Transformada de Fourier discreta (DFT)}
	%\subsection{Transformada de Fourier discreta (DFT)}
		
		Puesto que el cálculo de transformada de Fourier continua es relativamente costoso, para aquellos casos en que la función de entrada sea una secuencia finita de números reales o complejos se usa una generalización llamada transformada de Fourier discreta, la cual se define como una transformada de Fourier para análisis de señales de tiempo discreto y dominio finito, como suele ser la información almacenada en soportes digitales.
		
		Esta transformación ($F$) convierte una secuencia finita $X$ de $N$ números complejos en otra secuencia finita $Y$ de números complejos de igual longitud
		$$
			\ubmat{
				X = [x_{0}, x_{2}, \dots, x_{N-1}]
				& \mapsto &	
				Y = [y_{0}, y_{2}, \dots, y_{N-1}]
				& \backslash &	
				Y = F(X)
			}
		$$
		mediante la fórmula
		$$
			y_{k} = \sum_{n=0}^{N-1} x_{n} e^{-\frac{2 \pi i}{N}kn} \hspace{3em} k=0, 1, \dots, P-1
		$$
		y su inversa ($Y \mapsto X$) mediante
		$$
			x_{n} = \frac{1}{N} \sum_{k=0}^{N-1} y_{k} e^{\frac{2 \pi i}{N}kn} \hspace{3em} n=0, 1, \dots, P-1
		$$
		
		Otra forma de interpretarla es considerando los conjuntos $X$ e $Y$ como dos vectores columna y expresar la DFT como una matriz de Vandermonde (matriz que presenta una progresión geométrica en cada fila)
		$$
			F[j, k] = e^{(\frac{2 \pi i}{N})jk} = w_{N}^{jk} \hspace{3em} j, k = 0, 1, \dots, N-1 \hspace{1.5em} \backslash \hspace{1.5em} Y = F X
		$$
		y su inversa como
		$$
			F^{-1} = \frac{1}{N}F^{*} \hspace{1.5em} \backslash \hspace{1.5em} X = F^{-1} Y
		$$
		
		Avanzando un paso más es posible, mediante normalización, hacer que sea una transformación unitaria $U = \frac{1}{\sqrt{N}}F$ y $U^{-1} = U^{*}$ tal que $\abs{det(U)} = 1$
	

	\section{Transformada cuántica de Fourier (QFT)}

		Puesto que la transformada discreta actúa sobre un vector discreto y finito de números reales o complejos, y ya que un estado cuántico se representa mediante un vector discreto y finito de números complejos, esta transformación debiera poderse aplicar a un estado cuántico siempre que se pueda implementar en un circuito cuántico.
		
		Así, tenemos una transformación desde un estado de n qubits $\ketp{}$ representado por un vector de $N = 2^{n}$ números complejos, a otro $F\ketp{}$ que se corresponde con el primero, pero en el dominio de la frecuencia
		$$
			\ubmat{
				\ketp{} = \sum_{\substack{j=0}}^{N-1} a_{j} \ket{j}
				& \mapsto &	
				F\ketp{} = \sum_{\substack{k=0}}^{N-1} b_{k} \ket{k}
			}
		$$
		siendo (de forma análoga a la transformada discreta de Fourier)
		$$
			\ubmat{
				b_{k} = \frac{1}{\sqrt{N}} \sum_{j=0}^{N-1}a_{j}w^{jk}
				& & \wedge & &
				w = e^{\frac{2 \pi i}{N}}
			}
		$$
		por simplicidad se va a utilizar $w = w_{N}$ dejando implícito el término $N$ para cada caso, con el fin de no recargar tanto las ecuaciones.
		
		El término $\frac{1}{\sqrt{N}}$ sirve para normalizar el vector, pues un estado cuántico ha de ser un vector normalizado.
		
		Por ejemplo, para el caso de un estado de 2 qubits (N = 4)
		$$
			\ubmat{
				\ketp{} = [a_{0}, a_{1}, a_{2}, a_{3}]^{T} = a_{0} \ket{00} + a_{1} \ket{01} + a_{2} \ket{10} + a_{3} \ket{11}
				\\ \\
				F\ketp{} = [b_{0}, b_{1}, b_{2}, b_{3}]^{T} = b_{0} \ket{00} + b_{1} \ket{01} + b_{2} \ket{10} + b_{3} \ket{11}
			}
		$$
		calculando los valores del estado $F\ketp{}$ por separado
		$$
			\ubmat{
				b_{0} = \frac{1}{\sqrt{4}} \sum_{j=0}^{4-1}a_{j}w^{0j} = \frac{1}{2} \sum_{j=0}^{3}a_{j}w =
				\frac{1}{2} [a_{0}w^{0} + a_{1}w^{0} + a_{2}w^{0} + a_{3}w^{0}]
				\\ \\
				b_{1} = \frac{1}{\sqrt{4}} \sum_{j=0}^{4-1}a_{j}w^{1j} = \frac{1}{2} \sum_{j=0}^{3}a_{j}w^{j} =
				\frac{1}{2} [a_{0}w^{0} + a_{1}w^{1} + a_{2}w^{2} + a_{3}w^{3}]
				\\ \\
				b_{2} = \frac{1}{\sqrt{4}} \sum_{j=0}^{4-1}a_{j}w^{2j} = \frac{1}{2} \sum_{j=0}^{3}a_{j}w^{2j} =
				\frac{1}{2} [a_{0}w^{0} + a_{1}w^{2} + a_{2}w^{4} + a_{3}w^{6}]
				\\ \\
				b_{3} = \frac{1}{\sqrt{4}} \sum_{j=0}^{4-1}a_{j}w^{3j} = \frac{1}{2} \sum_{j=0}^{3}a_{j}w^{3j} =
				\frac{1}{2} [a_{0}w^{0} + a_{1}w^{3} + a_{2}w^{6} + a_{3}w^{9}]
			}
		$$
		es posible representar esta operación mediante matrices, así tenemos una matriz operador $F_{4}$ que se aplica al vector que representa el estado $\ketp{}$.
		$$
			F_{4} = \frac{1}{\sqrt{4}} \arr{
				w^{0} & w^{0} & w^{0} & w^{0}\\ 
				w^{0} & w^{1} & w^{2} & w^{3}\\ 
				w^{0} & w^{2} & w^{4} & w^{6}\\ 
				w^{0} & w^{3} & w^{6} & w^{9}\\ 
			} = \frac{1}{\sqrt{4}} \arr{
			1 & 1 & 1 & 1 \\ 
			1 & w^{1} & w^{2} & w^{3}\\ 
			1 & w^{2} & w^{4} & w^{6}\\ 
			1 & w^{3} & w^{6} & w^{9}\\ 
			}
		$$
		y gracias a que $w = e^{\frac{2 \pi i}{4}} = e^{\frac{\pi i}{2}}$, tenemos que para el caso de 2 qubits $w^{4} = e^{2 \pi} = 1$ y en consecuencia, por ejemplo, $w^{6} = w^{4}w^{2} = 1w^{2} = w^{2}$, lo que nos permite simplificar la matriz operador a
		$$
			F_{4} = \frac{1}{\sqrt{4}} \arr{
				1 & 1 & 1 & 1 \\ 
				1 & w^{1} & w^{2} & w^{3}\\ 
				1 & w^{2} & 1 & w^{2}\\ 
				1 & w^{3} & w^{2} & w^{1}\\ 
			}
		$$
		Por lo tanto
		$$
			F_{4}\ketp{} = \frac{1}{\sqrt{4}}
			\arr{
				1 & 1 & 1 & 1 \\ 
				1 & w^{1} & w^{2} & w^{3}\\ 
				1 & w^{2} & 1 & w^{2}\\ 
				1 & w^{3} & w^{2} & w^{1}\\ 
			} 
			\arr{a_{0} \\ a_{1} \\ a_{2} \\ a_{3}} = 
			\arr{b_{0} \\ b_{1} \\ b_{2} \\ b_{3}}
		$$
		
		Esta matriz operador es generalizable para cualquier N
		$$
			F_{N} = \frac{1}{\sqrt{N}} \arr{
				w^{0} & w^{0} & w^{0} & w^{0} & \cdots & w^{0} \\ 
				w^{0} & w^{1} & w^{2} & w^{3} & \cdots & w^{N-1} \\ 
				w^{0} & w^{2} & w^{4} & w^{6} & \cdots & w^{2(N-1)} \\ 
				w^{0} & w^{3} & w^{6} & w^{9} & \cdots & w^{3(N-1)} \\ 
				\vdots  & \vdots & \vdots & \vdots  & \ddots & \vdots \\ 
				w^{0} & w^{N-1} & w^{2(N-1)} & w^{3(N-1)} & \cdots & w^{(N-1)(N-1)}
			}
		$$
		y ademas se puede comprobar que es unitaria y por lo tanto reversible (siendo su adjunta su propia inversa) pues 
		$$
			FF^{\dagger} = I = F^{\dagger}F
		$$
		con lo que cumple dicha condición para ser aplicable en computación cuántica.
		
		
		Un caso que puede llevar a confusión es el caso de la transformada cuántica de Fourier para 1 qubit ($w = w_{2} = e^{\pi i}$), pues su operador es igual al de una puerta de Hadamard
		$$
			F_{2} = 
				\frac{1}{\sqrt{2}}
				\arr {
					w^{0} & w^{0} \\
					w^{0} & w^{1} 
				} = 
				\frac{1}{\sqrt{2}}
				\arr {
					1 & 1 \\
					1 & -1 
				} =
				H_{1}
		$$
		lo cual podría llevar a pensar que para obtener el operador para la QFT sobre 2 qubits, bastaría con hacer el producto tensorial del operador para un qubit consigo mismo como se haría para obtener una puerta de Hadamard para dos qubits, no obstante esto no sería correcto pues
		$$
			H_{2} 
			=
			H_{1} \otimes H_{1} 
			=
			\frac{1}{\sqrt{2}}
			\arr {
				1 & 1 \\
				1 & -1 
			} 
			\otimes
			\frac{1}{\sqrt{2}}
			\arr {
				1 & 1 \\
				1 & -1 
			} 
			=
			\frac{1}{\sqrt{4}} \arr{
				1 &  1 &  1 &  1 \\ 
				1 & -1 &  1 & -1 \\ 
				1 &  1 & -1 & -1 \\ 
				1 & -1 & -1 &  1 \\ 
			}
		$$
		pero en cambio el operador de la QFT para 2 qubits ($F_{4}$) siendo 
		$$
			w = w_{4} = e^{\frac{2 \pi i}{4}} = e^{\frac{\pi i}{2}}
		$$
		y por lo tanto 
		$$
			\ubmat{w^{0} = 1 & \hspace{2em} & w^{1} = i & \hspace{2em} & w^{2} = -1 & \hspace{2em} & w^{3} = -i}
		$$
		no es igual a la puerta de Hadamard para 2 qubits
		$$
			F_{4} = 
				\frac{1}{\sqrt{4}} \arr{
					1 & 1 & 1 & 1 \\ 
					1 & w^{1} & w^{2} & w^{3}\\ 
					1 & w^{2} & 1 & w^{2}\\ 
					1 & w^{3} & w^{2} & w^{1}\\ 
				} 
				=
				\frac{1}{\sqrt{4}} \arr{
					1 &  1 &  1 &  1 \\ 
					1 &  i & -1 & -i \\ 
					1 & -1 &  1 & -1 \\ 
					1 & -i & -1 &  i \\ 
				}
				\neq
				\frac{1}{\sqrt{4}} \arr{
					1 &  1 &  1 &  1 \\ 
					1 & -1 &  1 & -1 \\ 
					1 &  1 & -1 & -1 \\ 
					1 & -1 & -1 &  1 \\ 
				}
				=
				H_{2}
		$$
		pues la transformada cuántica de Fourier no puede construirse mediante el producto tensorial de transformadas parciales. Como se explica en la sección siguiente, el circuito de la transformada cuántica de Fourier para $n$ qubits se construye a partir del circuito para $n -1$ qubits añadiendo $n$ puertas lógicas extra (1 puerta de Hadamard de 1 qubit y $n -1$ puertas de desplazamiento de fase). 
		
		No obstante, en el caso de aplicarse a 1 solo qubit, la transformada cuántica de Fourier y la puerta de Hadamard son equivalente.
		
	\section{Circuito de la transformada cuántica de Fourier}
		
		Teniendo un operador representable mediante una matriz unitaria, falta ver si esta operación es implementable en un circuito cuántico.
		
		Para implementar una operación sobre n qubits en un circuito cuántico de forma sencilla, es importante conseguir una representación de la operación como el producto tensor de n qubits-únicos, de forma que pueda descomponerse en la aplicación secuencial de un conjunto de puertas cuánticas sobre cada qubit individual.
		
		Por comodidad en esta sección se va a usar la notación
		$$
			[0.x_{1}...x_{n}] = \sum_{k = 1}^{n} x_{k}2^{-k} = \frac{x_{1}}{2} + \frac{x_{2}}{2^{2}} + \dots + \frac{x_{n}}{2^{n}}
		$$
		
		En el caso de la transformada cuántica de Fourier, esta puede expresarse 
		$$
			F: \hspace{1em} \ket{x_{1}, \dots , x_{n}} \mapsto \frac{1}{\sqrt{2^{n}}}
			(\ket{0} + e^{2 \pi i [0.x_{n}]}\ket{1}) \otimes
			%(\ket{0} + e^{2 \pi i [0.x_{n-1}x_{n}}\ket{1}) \otimes
			\dots \otimes
			(\ket{0} + e^{2 \pi i [0.x_{1}x_{2} \dots x_{n}]}\ket{1})
		$$
		siendo por lo tanto el caso de dos qubits
		$$
			F\ket{x_{1}x_{2}} = 
			\frac{1}{\sqrt{4}}
			(\ket{0} + e^{2 \pi i [0.x_{2}]}\ket{1}) \otimes
			(\ket{0} + e^{2 \pi i [0.x_{1}x_{2}]}\ket{1})
		$$
		y el caso de tres qubits
		$$
			F\ket{x_{1}x_{2}x_{3}} = 
			\frac{1}{\sqrt{8}}
			(\ket{0} + e^{2 \pi i [0.x_{3}]}\ket{1}]) \otimes
			(\ket{0} + e^{2 \pi i [0.x_{2}x_{3}}\ket{1}]) \otimes
			(\ket{0} + e^{2 \pi i [0.x_{1}x_{2}x_{3}]}\ket{1})
		$$
		
		Por lo tanto
		$$
			F\ket{x_{1}, \dots , x_{n}} = \ket{y_{1}, \dots , y_{n}}
		$$
		tal que
		$$
			\begin{matrix}
				y_{1} = \frac{1}{\sqrt{2^{n}}}(\ket{0} + e^{2 \pi i [0.x_{n}]}\ket{1}) \\
				y_{2} = (\ket{0} + e^{2 \pi i [0.x_{n-1}x_{n}]}\ket{1}) \\
				\vdots \\
				y_{n} = (\ket{0} + e^{2 \pi i [0.x_{1}x_{2} \dots x_{n}]}\ket{1})
			\end{matrix}	
		$$
		($\frac{1}{\sqrt{2^{n}}}$ solo multiplica a uno de los qubits pues $\alpha (B \otimes C) = (\alpha B) \otimes C = B \otimes (\alpha C)$ siendo $\alpha$ un escalar y B y C matrices).

		Puesto que puede factorizarse como el producto tensor de n qubits, ahora queda ver si las operaciones sobre cada qubit son implementables. 
		
		En este caso cada una de las operaciones de los qubits puede ser implementada eficientemente usando una puerta Hadamard ($H$) y puertas controladas de desplazamiento de fase ($R_{\phi}$). El primer término ($x_{n}$) requiere una puerta Hadamard, el segundo término requiere una puerta Hadamard y una puerta controlada de desplazamiento de fase, el tercer término requiere una puerta de Hadamard y dos de desplazamiento de fase, y así consecutivamente añadiendo una puerta adicional de desplazamiento de fase para cada término respecto al anterior.
		
		Sumando las puertas para cada término 
		$$
			1 + 2 + \cdots + n = n(n+1) / 2 \Rightarrow O(n^{2})
		$$ 
		puertas, siendo $n$ el número de qubits. Esto es un incremento cuadrático de puertas cuánticas necesarias respecto al número de qubits
		
		En la figura \ref{pic:quantum_Fourier_transform_on_three_qubits_rec} se muestra el circuito de la QFT para 3 qubits ($n = 3 \Rightarrow 3(3+1)/2 = 6$ puertas cuánticas). En el puede observarse una estructura recursiva en la cual la linea de $\ket{y_{1}}$ (correspondiente a $\ket{x_{n}}$) se construye a partir de la salida de aplicar la QFT a los qubits $\ket{x_{1}x_{2}}$.
		
		\begin{figure}[ht!]
			\includegraphics[width=\textwidth]{Quantum_Fourier_transform_on_three_qubits_rec}
			\centering
			\caption{Circuito cuántico de la QFT para 3 qubits}
			\label{pic:quantum_Fourier_transform_on_three_qubits_rec}
		\end{figure}
		
		En la figura \ref{pic:quantum_Fourier_transform_on_n_qubits} se muestra el circuito de la QFT general para un número $n$ de qubits.
		
		\begin{figure}[ht!]
			\includegraphics[width=\textwidth]{Quantum_Fourier_transform_on_n_qubits}
			\centering
			\caption{Circuito cuántico de la QFT para $n$ qubits}
			\label{pic:quantum_Fourier_transform_on_n_qubits}
		\end{figure}
		
		
