

\section{Primera iteración - Implementación inicial}

	Para la implementación inicial se decidió no incluir ninguna mejora, por lo que el programa solo coge un estado cuántico, genera la matriz operador de la QFT completa y realiza la multiplicación, devolviendo el estado cuántico resultante.
	
	
	\subsection{Diseño}
	
		Esta primera implementación utiliza una interfaz sencilla, la cual recibe como parámetro el estado cuántico de entrada y devuelve el estado cuántico resultante.
		
		El número de qubits por palabra se obtiene del propio estado de entrada, pues este será un vector columna con $N = 2^{n}$ filas, siendo $n$ el número de qubits.
		
		El algoritmo recibe un estado cuántico, obtiene el número de qubits del estado, genera la matriz $F$ y la multiplica (por la izquierda) al estado cuántico recibido, revolviendo el resultado.	
	
	\subsection{Pruebas}
		
		Para probar esta implementación se han preparado manualmente unos estados de pocos qubits y su resultado tras aplicarle la transformada, para así poder comprobar que el resultado devuelto es correcto.
		
		La prueba más básica es comprobar que la transformada de 1 qubit tiene el mismo efecto que una puerta de Hadamard de 1 qubit.
	
	\subsection{Estudio de coste en memoria}
	
		El coste en memoria de este algoritmo es directamente dependiente del número de qubits utilizados en el estado. Esta dependencia es exponencial con base 2, pues tanto los estados como el operador son matrices de números complejos, necesitando $2^{n}$ números complejos para los estados y $(2^{n})^{2}$ para los operadores. De aquí en adelante para simplificar se usará $N = 2^{n}$, siendo $n$ el número de qubits.
		
		En python los números complejos internamente se representan como dos valores en coma flotante. Python implementa los valores en coma flotante como un \textit{double} de C (habitualmente codificado en binario usando la codificación IEEE 754 de 64 bits).
		
		\begin{itemize}
			
			\item La entrada es un estado cuántico de la librería de qutip. Internamente es una matriz columna de números complejos con $N$ filas.
			
			\item La salida, al igual que la entrada, es un estado cuántico de la librería de qutip, siendo internamente una matriz columna de números complejos con $N$ filas.
			
			\item El operador $F$ es una matriz cuadrada de rango $N$ de números complejos.
			
		\end{itemize}
		
		Tanto la entrada como la salida necesitan $N$ números complejos lo que supone un incremento lineal de memoria, pero el operador en cambio, al ser una matriz cuadrada, son $N^{2}$ números complejos y esto es un incremento cuadrático, por lo tanto la complejidad espacial teórica para este algoritmo es 
		$$
			2N + N^{2} = 2*2^{n} + (2^{n})^{2} = 2^{n+1} + 2^{2n}
		$$
		lo que supone una complejidad espacial exponencial respecto al número de qubits.
		
		Como puede comprobarse en la tabla la tabla \ref{tab:it1_theo_min_mem_usage}, mientras que el incremento del uso de memoria de los estados es asumible, no lo es así el del operador. Esto provocará que el sistema empiece a usar memoria de SWAP incrementando enormemente el tiempo de ejecución por la latencia del disco duro.
		
		\begin{table}
			\label{tab:it1_theo_min_mem_usage}
			\centering
			
			\begin{tabular}{| c | c | c |}

				\hline
				
				\textbf{Qubits} & \textbf{Estado (\textit{complex})} & \textbf{Operador (\textit{complex})} \\ \hline \hline
				
				1 & 2 & 4 \\ \hline
				2 & 4 & 16 \\ \hline
				3 & 8 & 32 \\ \hline
				4 & 16 & 64 \\ \hline
				5 & 32 & 128 \\ \hline
				\vdots & \vdots & \vdots \\ \hline
				10 & $2^{10} =$ & $2^{20} =$ \\ 
				 & 1024 & 1.048.576 \\ \hline
				\vdots & \vdots & \vdots \\ \hline
				20 & $2^{20} =$ & $2^{40} =$ \\ 
				 & 1.048.576 & 1.099.511.627.776 \\ \hline
			\end{tabular}
			
			\caption{Iteración 1 - Incremento mínimo teórico en el uso de memoria}
		\end{table}
		
		En el supuesto de que se contasen con los 2GB íntegros para ser usados por el programa para el contenido de las variables, tendríamos  $2GB = 2 * 2^{40} * 8 = 2^{44}bits$, y puesto que cada número complejo utiliza 2 números en coma flotante y estos suelen necesitar 64 bits, cada número complejo consideramos que precisa de $1complex = 2float = 2^{9}bits$, y por lo tanto disponemos de espacio para $2^{44} / 2^{9} = 2^{35}$ números complejos. Esto hace un máximo de 17 qubits
		$$
			2*2^{17} + (2^{17})^{2} = 2*2^{17} + 2^{34} = 17.180.131.328 < 2^{35} = 34.359.738.368
		$$
		mientras que con 18 qubits ya se pasaría con el tamaño del operador 
		$$
			(2^{18})^{2} = 2^{36} > 2^{35}
		$$
		
		Esos cálculos están realizados en el supuesto de disponer de 2GB enteros de memoria solo para el contenido de las variables, pero en la vida real la memoria es compartida entre el sistema operativo y los programas en ejecución. Ademas, aunque el proceso cuente con 2GB enteros, parte de estos se usarán para alojar el código a ser ejecutado, los indices con las direcciones de memoria dinámica, la pila de llamadas (stack) y el espacio para el contenido de las variables (heap).
		
		Empíricamente, el sistema operativo en reposo mantiene un uso aproximado de memoria RAM de medio GB (como puede observarse en la figura \ref{pic:it1_memory_usage_standby}), lo cual reduce la memoria disponible aproximadamente a 1536 MB ($2^{43}+2^{42}$ bits, que son $2^{34}+2^{33}$ números complejos).
		
		\begin{figure}[ht!]
			\includegraphics[width=\textwidth]{it1_memory_usage_standby}
			\centering
			\caption{Primera iteración - Consumo de recursos del SO}
			\label{pic:it1_memory_usage_standby}
		\end{figure}
		
		Otro detalle a tener en cuenta es que en el algoritmo, el operador primero se construye como una matriz de la librería \textit{numpy}, para luego transformarse en un objeto de la librería cuántica de \textit{qutip}, por lo que en un momento dado el operador se encuentra duplicado en memoria, aumentando el consumo real frente al consumo teórico mínimo calculado previamente. Así, en el momento de máximo uso de la memoria, se precisa de espacio para 
		$$
			2N + 2N^{2}
		$$ 
		(frente a los $2N + N^{2}$ calculados previamente).
		
		Con estas nuevas restricciones, el número máximo teórico de qubits antes de que haya problemas de memoria es de 16 qubits
		$$
			2*2^{16} + 2*(2^{16})^{2} = 2*2^{16} + 2*2^{32} = 8.59.065.664 < 2^{34}+2^{33} = 25.769.803.776
		$$
		mientras que con 17 qubits ya se pasaría con el tamaño del operador en el momento que esta duplicado
		$$
			2*(2^{17})^{2} = 2*2^{34} = 2^{35} > 2^{34}+2^{33}
		$$
		Con todo y con eso, durante la ejecución del sistema, aún hay más factores a tener en cuenta que consumen memoria, como son el propio shell desde el que se ejecuta el sistema, el interprete de python usado para la ejecución, el código propio y de librerías que ha de cargarse, el uso de espacio adicional de las estructuras de memoria utilizadas (por ejemplo un objeto de \textit{qutip}no es solo la matriz interna que lo representa) y el uso de memoria extra necesario para las operaciones, conllevan que le número máximo de qubits con los que se puede trabajar antes de tener problemas de memoria es inferior al esperado.
	
		En concreto como puede verse en las figuras \ref{pic:it1_memory_12_qubits} y \ref{pic:it1_memory_full_break}, ejecutando el sistema con 12 qubits este se ejecuta satisfactoriamente, estando siendo usados un total de 1'9 GB de memoria RAM, pero al tratar de ejecutar el programa para 13 qubits, la librería de \textit{scipy} utilizada por \textit{numpy} lanza una excepción debido a falta de memoria al haber consumido tanto la RAM como gran parte de la de SWAP. Un análisis instrucción por instrucción del código demuestra que el punto donde salta esa excepción es la instrucción ``\textit{F = qutip.Qobj(F)}'' que es cuando se transforma la matriz de \textit{numpy} F con los valores del operador en un objeto operador de \textit{qutip}. Ese punto es también donde la ejecución con 12 qubits alcanzó el máximo uso de memoria.
		
		\begin{figure}[ht!]
			\includegraphics[width=\textwidth]{it1_memory_12_qubits}
			\centering
			\caption{Primera iteración - Ejecución con 12 qubits}
			\label{pic:it1_memory_12_qubits}
		\end{figure}
		
		\begin{figure}[ht!]
			\includegraphics[width=\textwidth]{it1_memory_full_break}
			\centering
			\caption{Primera iteración - Ejecución con 13 qubits}
			\label{pic:it1_memory_full_break}
		\end{figure}
		
		El consumo real de memoria por parte unicamente del proceso se ha comprobado con la llamada al sistema proporcionada por la propia librería de python3.4. Esto puede observarse en la tabla \ref{tab:it1_resource_usage}. Como se puede observar, hay un mínimo consumo por parte del proceso de casi 42 MB, lo cual es suficiente hasta que se opera con 7 qubits, momento a partir del cual el incremento en memoria necesario crece de forma exponencial \todo{interpolar los valores y mostrar dicha interpolación}
		
	\subsection{Estudio de coste en tiempo}
		
		El estudio del tiempo se ha llevado a cabo de forma teórica mirando la complejidad ciclomática y de forma empírica tomando el tiempo interno del sistema antes y después de ejecutar el algoritmo para calcular su diferencia.
		
		Así como el incremento en el uso de memoria crece de forma exponencial al número de qubits, también así lo hace el incremento en tiempo de ejecución, pues no solo ha de generarse el operador recorriendo una matriz cuadrada con la consiguiente complejidad cuadrática, sino que luego ha de realizarse una multiplicación de matrices al aplicar el operador al estado. 
		
		Por ello y puesto que el tamaño de las matrices es exponencial respecto al número de qubits, la complejidad se dispara, siendo la de construir del operador
		$$
			O(N^{2}) = O((2^{n})^{2}) = O(2^{2n})
		$$ 
		y la de aplicar el operador al estado
		$$
			O((2N)^{2}) = O((2(2^{n}))^{2}) = O((2^{n + 1})^{2}) = O(2^{2n + 2})
		$$
		lo que supone una complejidad total de ambas operaciones respecto al número de qubits ($n$) de
		$$
			O( 2^{2n} + 2^{2n + 2})
		$$
		
		Empíricamente esto se ha comprobado realizado 10 ejecuciones para medir el tiempo utilizado y se ha calculado su media y varianza (truncadas a 5 decimales). Esto se ha llevado a cabo para estados de diferentes tamaños con el número de qubits variando entre 1 y 15. Los resultados comparativos (junto con el máximo de memoria usada por el proceso) pueden observarse en la tabla \ref{tab:it1_resource_usage}. En caso de que la memoria del sistema no fuera suficiente para ejecutar el algoritmo se muestra mediante ``-'' en las casillas de tiempo correspondientes.
		
		\begin{table}
			\label{tab:it1_resource_usage}
			\centering
			
			\begin{tabular}{| c | c | c | c |}
				
				\hline
				
				\textbf{Qubits} & \textbf{Tiempo (s)} & \textbf{Varianza (s)} & \textbf{Memoria (MB)} \\ \hline \hline
				
				\input{tables/it1_time_measure}
				
			\end{tabular}
			
			\caption{Iteración 1 - Comparativa del tiempo requerido}
		\end{table}
	
		Los resultados observados son acordes a los estimados teóricamente \todo{Gráfica interpolada}
	
	
	\subsection{Conclusiones}
		
		El algoritmo ha sido implementado de forma efectiva, pero dista mucho de ser eficiente. Sobretodo a nivel de memoria, que impone un límite superior al tamaño de los datos de entrada. 
		
		El consumo de memoria se concentra en el operador, siendo este una matriz de cuadrada, y teniendo que estar duplicada en el momento de convertirla a un objeto de \textit{qutip} para operar con el estado de entrada. Se probó a construir el operador directamente sobre un objeto de \textit{qutip}, pero resulto en un gran aumento del tiempo de ejecución, pasando el operar con un estado de 8 qubits a tardar lo mismo que antes con uno de 10, por lo que al ser una implementación inicial que será refinada en un futuro, se decidió seguir construyendo contenido del operador como una matriz aparte para luego transformarlo a un objeto de \textit{qutip}.
		
		En el aspecto temporal el consumo es considerable, y gran parte del tiempo se emplea en crear el operador, siendo necesario para cada posición de este una operación exponencial cuyo exponente es un número complejo.
		
		
		
	