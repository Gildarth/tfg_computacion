\section{\textit{time\_opt\_2}: Ejecución multithread}

	En esta optimización se va a partir de \textit{mem\_opt\_3} y se va a paralelizar. 
	
	\textit{mem\_opt\_3} es especialmente fácil de paralelizar, pues calcula cada elemento del estado resultante por separado y los agrupa. Por ello, se puede dividir el procesado de estos elementos entre distintos \textit{threads} (hilos de ejecución) para finalmente juntar todos los resultados. Con ello, se espera acelerar considerablemente el cálculo del resultado, más a cuantos más núcleos tenga el equipo donde se ejecute, sin reducir por ello el número máximo de qubits operables (24 en \textit{mem\_opt\_1}).
	
	A esta implementación se le ha asignado el identificador \textit{time\_opt\_2}.
	
	
	\subsection{Diseño}
		
		La arquitectura utilizada para esta paralelización es un \textit{Master/Slave}.
		
		La idea original era utilizar concurrencia dentro de un mismo proceso, teniendo el estado inicial y el resultado como datos globales compartidos para cada \textit{thread}, pero eso ha resultado imposible por una limitación impuesta por el propio \textit{python}, en el cual aunque existe la clase \textit{Thread} dentro de la librería \textit{Threading} (https://docs.python.org/3.4/library/threading.html), esta no permite aprovecharse de los procesadores de varios núcleos pues un proceso del intérprete de \textit{python} solo puede ejecutarse en un núcleo y por lo tanto aunque se creen \textit{threads} todos competirán por el mismo núcleo, nunca ejecutándose realmente en paralelo.
	
		Para solventar este problema se utiliza una aproximación de varios procesos hijos paralelos usando la librería de \textit{multiprocessing}, incluida en el propio lenguaje. Dicha librería permite, a alto nivel, generar un \textit{pool} de procesos, mandarles ejecutar a cada uno una función concreta con unos parámetros y recibir el resultado de la ejecución cuando termine. 
		
		Para usar dicha librería se hizo una función que recibe por parámetro el estado de entrada y lo necesario para saber que rango de valores del resultado debe calcular. En el método de ejecución de la transformada (que hace el papel de \textit{Master} representado en el diagrama de la figura \ref{pic:time_opt_2_master_flow_diagram}), se generan un \textit{pool} con tantos procesos (\textit{Slaves}) como núcleos tenga el sistema y se manda a cada proceso calcular una parte del resultado mediante la función mencionada (representada en el diagrama de la figura \ref{pic:time_opt_2_slave_flow_diagram}). Una vez todos los procesos han terminado el \textit{Master} los resultados y se juntan en orden, formando así el estado resultante de aplicar la transformación.
		
		
		Unos diagramas de flujo del algoritmo con los cambios principales respecto a \textit{mem\_impl\_3} en cajas rojas puede observarse en la figura \ref{pic:time_opt_2_master_flow_diagram}, la cual se corresponde al proceso \textit{Master}, y la figura \ref{pic:time_opt_2_slave_flow_diagram} la correspondiente al diagrama de flujo de los procesos \textit{Slave}
		
		\begin{figure}[ht!]
			\includegraphics[width=\textwidth]{time_opt_2_master_flow_diagram}
			\centering
			\caption{\textit{time\_opt\_2} - Master - Diagrama de flujo - Cambios}
			\label{pic:time_opt_2_master_flow_diagram}
		\end{figure}
		
		\begin{figure}[ht!]
			\includegraphics[width=20em]{time_opt_2_slave_flow_diagram}
			\centering
			\caption{\textit{time\_opt\_2} - Slave - Diagrama de flujo}
			\label{pic:time_opt_2_slave_flow_diagram}
		\end{figure}
		
		Por ello, se ha de modificar el método de medición de la memoria consumida. Convenientemente se usa una función de la librería ofrecida por \textit{python}, la cual devuelve el máximo de memoria usada por el proceso actual, pero que modificando una constante que se le pasa por parámetro, devuelve la memoria máxima consumida por los procesos hijo del proceso actual. Por lo tanto, se añadió una nueva llamada a la función con la constante para que devuelva la memoria de los hijos y se sumo a la del padre, siendo dicha suma el resultado de la medición.

	\subsection{Estudio de coste espacial - Teórico}
		
		El coste en memoria se esperaba que fuera superior al de la implementación de partida (sobretodo para pocos qubits), pues cada proceso hijo es un proceso independiente y ``adquiere'' una cantidad mínima de memoria al iniciarse aunque esta no se use. Aun así, puesto que no se transforma a un objeto de qutip el operador ni el resultado en los procesos \textit{Slave}, estos no debieran tener un consumo muy excesivamente grande en comparación con el total, siendo el consumo de memoria de estos solo el de la matriz del estado de entrada, la matriz operador y la matriz con la parte del resultado correspondiente. Eso es
		$$
			(N + N + N / p) \times p = (\frac{pN + pN + N}{p})p = pN + pN + N = N (2p + 1)
		$$
		siendo $p$ el número de procesos \textit{Slave}. Si esto lo unimos al consumo del padre, el cual contiene los objetos \textit{qutip} de entrada y de salida ($2N$), así como la matriz del estado de entrada que se manda a los procesos y la del resultado donde se recogen los resultados de cada proceso ($2N$), tenemos un total de
		$$
			2N + 2N + N (2p + 1) = 4N + N (2p + 1) = N (2p + 1 + 4) = N (2p + 5)
		$$
		frente a la complejidad de $4N$ que tiene \textit{mem\_impl\_3}.
	
	
	\subsection{Estudio de coste espacial - Práctico}
		
		En lo referente al número máximo de qubits operables, el resultado es le mismo que en la implementación de partida (\textit{mem\_opt\_3}) como se observa en la tabla \ref{tab:time_opt_2_memory_limits_comparation}, pudiéndose operar hasta el máximo de 24 qubits como se muestra en la tabla comparativa \ref{tab:time_opt_2_memory_limits_comparation}, donde ademas se observa que el incremento del uso de memoria respecto a la implementación de partida es casi nulo, siendo de 1400 MB para 24 qubits.
		
		Sorprendentemente al ejecutar el código, aunque cada núcleo trabaja al 100\% de su capacidad, el consumo de memoria apenas aumenta y, al mostrar por terminal la memoria usada por padre e hijos, la de los hijos resulta ser 0.0 MB, siendo la ocupada por el padre íntegramente la usada por el programa. Esto se confirma al observar en el administrador del sistema que el consumo de memoria aumenta una cantidad similar a la ocupada por el padre. 
		
		Pese a que los valores de la medición y el administrador del sistema coinciden, se realizo un segundo método para medir la memoria consumida el cual la calcula mediante llamadas al sistema encapsuladas por al librería \textit{psutil}, iterando sobre la información de los procesos \textit{slave} (a la que accede desde la información del proceso \textit{master}) y suma la memoria consumida por cada uno.		
		
		En la imagen \ref{pic:time_opt_2_memory_measure_tipe_1} se puede observar el estado del sistema antes, durante y después de la ejecución del algoritmo para 14 qubits, así como la información mostrada por terminal de los dos métodos descritos para medir la memoria consumida. Se puede comprobar que el sistema funciona de forma paralela pues los 4 núcleos trabajan a plena capacidad, y el PID de los procesos \textit{slave} (children) es diferente al del proceso \textit{master} (father). Así mismo, se puede observar que la memoria ocupada antes de la ejecución es de 517 MiB y durante su ejecución es 559 MiB, lo que hace una diferencia de 42 MiB $ \approx $ 40 MB. El método de medida proceso por proceso calcula un consumo total de memoria de 180 MB lo cual no se corresponde con los datos del monitor del sistema. Por otro lado, el método que trata los hijos como un todo, considera que estos tienen un consumo de 0.0 MB y el total es el consumo del padre, siendo este 47.87 MB, resultado que esta muchísimo más cerca de lo observado en el monitor del sistema.
		
		La conclusión a la que se llego después de investigar, es que la librería de python, pese a que crea procesos diferentes, debe hacer algo con la memoria de manera que los hijos acceden a la memoria del padre en lugar de a un espacio de memoria propio, y por ende la memoria que ocupan es de 0.0, siendo gran parte de esta compartida entre los 4 (por ejemplo el array con el estado de entrada que se le pasa como parámetro a la función que ejecuta cada proceso \textit{Slave} o el código del propio intérprete de \textit{python}). Esto explicaría que la medir con la librería de \textit{python} desde el proceso \textit{Master} (que es el proceso padre) se considere que los hijos no tienen consumo de memoria, pero que a la hora de consultar el consumo desde el propio proceso \textit{Slave} (proceso hijo) se obtenga como resultado un consumo de 33 MB por proceso \textit{Slave} (valor mostrado en el terminal de abajo de la figura \ref{pic:time_opt_2_memory_measure_tipe_1}), ya que la información de que esa memoria en realidad es compartida debe de ser accesible solo desde el proceso \textit{Master}.
		
		Se observa también, mediante el nuevo método de medición por procesos, un proceso hijo extra con un consumo de 0.0 MB, lo cual no era esperado. Este proceso se ha comprobado que aparece también para aquellas implementaciones no paralelas, por lo que se ha deducido que no tiene que ver con el \textit{pool} de procesos usado ni con estos, y por ello no se le ha dado mayor importancia.
		
		\begin{figure}[ht!]
			\includegraphics[width=\textwidth]{time_opt_2_measure_1_all}
			\centering
			\caption{\textit{time\_opt\_2} - Medición de memoria con el \textit{core} de \textit{python}}
			\label{pic:time_opt_2_memory_measure_tipe_1}
		\end{figure}
		
		Con esto se ha mantenido el objetivo de paralelizar la ejecución y seguirse pudiendo operar con hasta 24 qubits, sin que por ello se haya aumentado en exceso el consumo de memoria.
		
		\begin{table}
			
			\centering
			
			\begin{tabular}{| c | c | c |}
				
				\hline
				
				\textbf{n} & \textbf{mem\_opt\_3} & \textbf{time\_opt\_2}  \\ \hline \hline
				
				\input{tables/time_opt_2_memory_limits_comparation}
				
			\end{tabular}
			\caption{time\_opt\_2 - Comparativa del uso de memoria (MB)}
			\label{tab:time_opt_2_memory_limits_comparation}
		\end{table}
	
	\subsection{Estudio de coste temporal - Teórico}
	
		El comportamiento esperado para esta implementación es un aumento en el tiempo de ejecución para estados pequeños (para los cuales es mas costoso el lanzar los procesos que realizar la operación directamente), y una reducción creciente y muy considerable según se aumenta el número de qubits. 
		
		Esta reducción debiera ser proporcional al número de procesos y en un mundo ideal el tiempo de ejecución sería $1/P$ del tiempo original, siendo $P$ el número de procesos, pero en realidad se espera que esté un poco por encima pues hay que añadir por lo menos el tiempo de creación de procesos, paso de los datos de entrada, recepción de los datos de salida y conversión a objeto de \textit{qutip}, funciones las cuales realiza el proceso \textit{Master}.
		
		Las modificaciones del algoritmo solo son en lo referente al bucle principal, estando esta ahora dividido entre $P$ procesos. Por este motivo, la complejidad ciclomática se ve modificada con respecto al bucle principal que es el término $N^{2}$ de la complejidad ciclomática de \textit{mem\_opt\_3} ($O(2N^{2} + 2N)$). 
		
		Este $O(N^{2})$ original se descompone en un primer bucle que cuenta los términos del resultado a calcular y en un segundo bucle anidado para generar el operador parcial para el término del resultado correspondiente. El bucle interno no se ha modificado ($O(N)$), pero el bucle exterior se ha dividido equitativamente entre los $P$ procesos ($O\frac{N}{P})$). Todo ello repetido dentro de los P procesos, pero aunque se realiza $P$ veces, se hace de forma simultánea y por lo tanto no se ha contado en la complejidad ciclomática, la cual es
		$$
			O(\frac{N}{P} N) + O(2N) = O(\frac{N^{2}}{P} + 2N)
		$$
		que coincide con lo teorizado en los párrafos anteriores sobre que el consumo temporal es inversamente proporcional al número de procesos de que se disponga.
		
	\subsection{Estudio de coste temporal - Práctico}
		
		Con todo esto, en la tabla \ref{tab:time_opt_2_resource_usage} se puede observar los resultados de las mediciones con hasta 15 qubits, en la tabla \ref{tab:time_opt_2_time_comparation} una comparativa con \textit{mem\_opt\_3} y en \ref{tab:time_opt_time_usage_comparation} la comparativa con otras implementaciones de tiempo de ejecución, donde se observa la esperada reducción drástica y se comprueba que según va aumentando el número de qubits, el tiempo requerido tiende a estar entre $1/3 \approx 33\%$ y $2/5 = 40\%$ del requerido por \textit{mem\_opt\_3}, lo cual concuerda con lo que se esperaba pues al ser un sistema con 4 núcleos se estimó que requeriría algo mas de $1/4 = 25\%$ del tiempo de \textit{mem\_opt\_3}.
		
		También se puede observar, en concordancia con lo esperado, un tiempo mínimo de ejecución alrededor de los 0.05 segundos, lo cual se debe al tiempo necesario para la creación de procesos, distribución del trabajo y recogida de resultados.
		
		\begin{table}
			
			\centering
			
			\begin{tabular}{| c | c | c |}
				
				\hline
				
				\textbf{n} & \textbf{mem\_opt\_3} & \textbf{time\_opt\_2}  \\ \hline \hline
				
				\input{tables/time_opt_2_time_comparation}
				
			\end{tabular}
			\caption{time\_opt\_2 - Comparativa del consumo de tiempo (s)}
			\label{tab:time_opt_2_time_comparation}
		\end{table}
		
		
	
	\subsection{Conclusiones}
	
		Se ha conseguido paralelizar la implementación de \textit{mem\_opt\_3} reduciendo considerablemente el tiempo de ejecución, manteniendo el número máximo de qubits operables.
		
		Además, para 24 qubits el consumo de memoria es prácticamente igual al de \textit{mem\_opt\_3}, lo que refuerza la conclusión de que python realiza memoria compartida entre procesos y que el grueso del consumo de memoria esta en los objetos de qutip y no en las matrices individuales.
	
	
		\begin{table}
			
			\centering
			
			\begin{tabular}{| c | c | c | c |}
				
				\hline
				
				\textbf{Qubits} & \textbf{Memoria (MB)} & \textbf{Tiempo (s)} & \textbf{Varianza (s)} \\ \hline \hline
				
				\input{tables/time_opt_2_time_measure}
				
			\end{tabular}
			
			\caption{\textit{time\_opt\_2} - Resultados de las mediciones}
			\label{tab:time_opt_2_resource_usage}
		\end{table}
	