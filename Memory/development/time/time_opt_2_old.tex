\subsection{Ejecución multithread}

En esta optimización se va a partir de \textit{mem\_opt\_1} y se va a paralelizar. 

\textit{mem\_opt\_1} es especialmente fácil de paralelizar, pues calcula cada elemento del estado resultante por separado y los agrupa, por ello, se puede dividir el procesado de estos elementos entre distintos \textit{threads} (hilos de ejecución) para finalmente juntar todos los resultados. Con ello, se espera acelerar considerablemente el cálculo del resultado, más a cuantos más núcleos tenga el equipo donde se ejecute, sin reducir por ello el número máximo de qubits operables (24 en \textit{mem\_opt\_1}).

A esta implementación se le ha asignado el identificador \textit{time\_opt\_2}.


\subsubsection{Diseño}

La arquitectura utilizada para esta paralelización es un \textit{Master/Slave}.

La idea original era utilizar concurrencia dentro de un mismo proceso, teniendo el estado inicial y el resultado como datos globales compartidos para cada \textit{thread}, pero eso ha resultado imposible por una limitación impuesta por el propio \textit{python}, en el cual aunque existe la clase \textit{Thread} dentro de la librería \textit{Threading} (https://docs.python.org/3.4/library/threading.html), esta no permite aprovecharse de los procesadores de varios núcleos pues un proceso del intérprete de \textit{python} solo puede ejecutarse en un núcleo y por lo tanto aunque se creen \textit{threads} todos competirán por el mismo núcleo, nunca ejecutándose realmente en paralelo.

Para solventar este problema se utiliza una aproximación de varios procesos hijos paralelos usando la librería de \textit{multiprocessing}, incluida en el propio lenguaje. Dicha librería permite, a alto nivel, generar un \textit{pool} de procesos, mandarles ejecutar a cada uno una función concreta con unos parámetros y recibir el resultado de la ejecución cuando termine. 

Usando dicha librería se hizo una función que recibe por parámetro el estado de entrada y lo necesario para saber que rango de valores del resultado debe calcular. En el método de ejecución de la transformada (que hace el papel de \textit{Master}), se generan un \textit{pool} con tantos procesos (\textit{Slaves}) como núcleos tenga el sistema y se manda a cada proceso calcular una parte del resultado mediante la función mencionada. Una vez todos los procesos han terminado se recogen los resultados y se juntan en orden, formando así el estado resultante de aplicar la transformación.

\subsubsection{Pruebas}

Se han realizado pruebas funcionales comparando el resultado del algoritmo con el de aplicar el operador generado por \textit{qutip} (una vez redondeado) sobre el mismo estado generado aleatoriamente, para estados de tamaño entre 1 y 12 (máximo admitido por el operador de \textit{qutip}).

Como pruebas no funcionales, se realizaron mediciones tanto de tiempos de ejecución como de memoria utilizada, y se realizó un análisis sobre dichos resultados.

\subsubsection{Estudio de coste en memoria}

El coste en memoria se esperaba que fuera superior al de la implementación de partida (sobretodo para pocos qubits), pues cada proceso hijo es un proceso independiente y ``adquiere'' una cantidad mínima de memoria al iniciarse aunque esta no se use.

Por ello, se ha modificado el método que mide la memoria usada para que sume la usada por los hijos a la usada por el padre:

\begin{verbatim}
import resource

def get_mem_used_python_lib():
father = resource.getrusage(resource.RUSAGE_SELF)
.ru_maxrss
children = resource.getrusage(resource.RUSAGE_CHILDREN)
.ru_maxrss
print ("\nMeasurement  with Python core\n")
print ("\tFather - {0}\n\n\t\tChildrens - {1}\n\n"
.format(father/1024, children/1024))
return father + children
\end{verbatim}

Sorprendentemente al ejecutar el código, aunque cada núcleo trabaja al 100\% de su capacidad, el consumo de memoria apenas aumenta y, al mostrar por terminal la memoria usada por padre e hijos, la de los hijos resulta ser 0.0 MB, siendo la ocupada por el padre íntegramente la usada por el programa. Esto se confirma al observar en el administrador del sistema que el consumo de memoria aumenta una cantidad similar a la ocupada por el padre. Por ello, se realizo un segundo método para medir la memoria, el cual la calcula mediante llamadas al sistema encapsuladas por al librería \textit{psutil} iterando sobre los procesos \textit{slave} a (los que accede desde la información del proceso \textit{master}) y muestra la información de estos:

\begin{verbatim}
import psutil

def get_mem_used():
current_process = psutil.Process(os.getpid())
mem = current_process.memory_info().rss
print("\nMeasurement process by process\n")
print ("\tFather - PID ", os.getpid(), " - ", 
current_process.memory_info().rss/ (1024 * 1024), "\n")
for child in current_process.children(recursive=True):
mem += child.memory_info().rss
print ("\t\tChildren - PID ", child.pid, " - ", 
child.memory_info().rss/ (1024 * 1024))
print ("\n\tTotal - ", mem/(1024 * 1024), "\n\n")
return mem/1024
\end{verbatim}

En la imagen \ref{pic:time_opt_2_memory_measure_tipe_1} se puede observar el estado del sistema antes, durante y después de la ejecución del algoritmo para 14 qubits, así como la información mostrada por terminal de los dos métodos descritos para medir la memoria consumida. Se puede comprobar que el sistema funciona de forma paralela pues los 4 núcleos trabajan a plena capacidad, y el PID de los procesos \textit{slave} (children) es diferente al del proceso \textit{master} (father). Así mismo, se puede observar que la memoria ocupada antes de la ejecución es de 517 MiB y durante su ejecución es 559 MiB, lo que hace una diferencia de 42 MiB $ \approx $ 40 MB. El método de medida proceso por proceso calcula un consumo total de memoria de 180 MB lo cual, viendo las gráficas del sistema, no tiene mucho sentido. Por otro lado, el método que trata los hijos como un todo, considera que estos tienen un consumo de 0.0 MB y el total es el consumo del padre siendo 47.87 MB en total, resultado que esta muchísimo más cerca de lo observado en el monitor del sistema. 

Se observa también, mediante el nuevo método de medición por procesos, un proceso hijo extra con un consumo de 0.0 MB, lo cual no era esperado. Este proceso se ha comprobado que aparece también para aquellas implementaciones no paralelas, por lo que se ha deducido que no tiene que ver con el \textit{pool} de procesos usado ni con estos, y por ello no se le ha dado mayor importancia.

\begin{figure}[ht!]
	\includegraphics[width=\textwidth]{time_opt_2_measure_1_all}
	\centering
	\caption{\textit{time\_opt\_2} - Medición de memoria con el \textit{core} de \textit{python}}
	\label{pic:time_opt_2_memory_measure_tipe_1}
\end{figure}

En lo referente al número máximo de qubits operables, el resultado es le mismo que en la implementación de partida (\textit{mem\_opt\_1}), pudiéndose operar hasta el máximo de 24 qubits como se muestra en la tabla comparativa  \ref{tab:time_opt_memory_limits_comparation}, donde ademas se observa que el incremento del uso de memoria respecto a la implementación de partida es casi nulo \todo{Lo cual no tiene maldito sentido}, siendo de 1400 MB para 24 qubits cuando en el monitor de sistema se observa claramente que llena tanto la memoria RAM como la de SWAP \todo{captura de pantalla}.

Con esto se ha mantenido el objetivo de paralelizar la ejecución y seguirse pudiendo operar con hasta 24 qubits.

\subsubsection{Estudio de coste en tiempo}

El comportamiento esperado para esta implementación es un aumento en el tiempo de ejecución para estados pequeños (para los cuales es mas costoso el lanzar los procesos que realizar la operación directamente), y una reducción creciente y muy considerable según se aumenta el número de qubits. 

Esta reducción debiera ser proporcional al número de procesos y en un mundo ideal el tiempo de ejecución seria de $1/P$ del tiempo original, siendo $P$ el número de procesos, pero en realidad se espera que este un poco por encima pues hay que añadir por lo menos el tiempo de creación de procesos, paso de los datos de entrada y recepción de los datos de salida.

Con todo esto, en la tabla \ref{tab:time_opt_2_resource_usage} se puede observar los resultados de las mediciones para hasta 15 qubits y en \ref{tab:time_opt_time_usage_comparation} la comparativa con otras implementaciones, donde se observa una reducción drástica y se comprueba que según va aumentando el número de qubits, tendiendo la relación entre el tiempo de ejecución de esta implementación a algo más de $2/5$ (40\%) de la de \textit{mem\_opt\_1} (pese a que se esperaba fuera algo superior a $1/4$ al ser 4 núcleos lo que tiene el sistema).


\subsubsection{Conclusiones}

Se ha conseguido paralelizar la implementación de \textit{mem\_opt\_1} reduciendo considerablemente el tiempo de ejecución manteniendo el número máximo de qubits operables, pero ha habido varios contratiempos a la hora de medir el consumo de memoria, no pudiéndose asegurar que sea correcto, pues aunque el monitor de sistema diga que por ejemplo, como se muestra en la figura \ref{pic:time_opt_2_memory_measure_tipe_1}, el consumo para 14 qubits no llegue a 50 MB, al estarse usando 5 procesos (1 \textit{master} y 4 \textit{slaves}), el consumo de memoria debiera ser superior a 100 MB pues el espacio mínimo reservado para un interprete de \textit{python} esta por encima de los 30 MB. 

Además, para 24 qubits el consumo es prácticamente igual al de \textit{mem\_opt\_1} y eso tampoco debiera ser así, pues cada proceso \textit{slave} contiene una matriz fila con el operador parcial (haciendo un total de 4 matrices en esta caso) mientras que en \textit{mem\_opt\_1} solo hay una de esas matrices, la cual es responsable de un porcentaje considerable del consumo de memoria.

No obstante, el objetivo de esta implementación no es controlar el consumo de memoria, sino reducir el tiempo de ejecución de la mejor implementación conseguida en el refinamiento del consumo de memoria sin que se reduzca el número máximo de qubits operables, lo cual se considera se ha conseguido con creces al haber reducido el tiempo de ejecución a alrededor de 2/5 del tiempo original.


\begin{table}
	
	\centering
	
	\begin{tabular}{| c | c | c | c |}
		
		\hline
		
		\textbf{Qubits} & \textbf{Memoria (MB)} & \textbf{Tiempo (s)} & \textbf{Varianza (s)} \\ \hline \hline
		
		\input{tables/time_opt_2_time_measure}
		
	\end{tabular}
	
	\caption{\textit{time\_opt\_2} - Resultados de las mediciones}
	\label{tab:time_opt_2_resource_usage}
\end{table}
