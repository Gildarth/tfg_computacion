\chapter{Implementación base}
\minitoc
\label{chap:base_impl}

Como punto inicial, se decidió hacer un programa lo más sencillo posible que coja un estado cuántico, genere la matriz operador de la QFT completa y realice la multiplicación, devolviendo el estado cuántico resultante.

%\clearpage
\section{\textit{base\_impl}: Implementación base}

	%Para la implementación base se decidió hacer un programa lo más sencillo posible que coja un estado cuántico, genere la matriz operador de la QFT completa y realice la multiplicación, devolviendo el estado cuántico resultante.
	
	A esta implementación se le ha asignado el identificador \textit{base\_impl}.
	
	En la tabla \ref{tab:base_impl_resource_usage} se observan los resultados obtenidos de las mediciones al ejecutar el algoritmo con hasta 15 qubits y en las tablas \ref{tab:mem_opt_memory_limits_comparation} y \ref{tab:mem_opt_time_usage_comparation} dos comparativas de memoria y tiempo con otras implementaciones.
	
	\subsection{Diseño}

		Para la implementación se decidió extraer como constante parte del exponente ($2 \pi i / N$), calculándolo de esta manera solo una vez en lugar de una vez por término del operador.
		
		Un diagrama de flujo del algoritmo puede observarse en la figura \ref{pic:base_impl_flow_diagram}
		
		\begin{figure}[ht!]
			\includegraphics[width=20em]{base_impl_flow_diagram}
			\centering
			\caption{\textit{base\_impl} - Diagrama de flujo}
			\label{pic:base_impl_flow_diagram}
		\end{figure}
		
	\subsection{Estudio de coste espacial - Teórico}
	
		El coste en memoria de este algoritmo es directamente dependiente del número de qubits utilizados en el estado. Esta dependencia es exponencial con base 2, pues tanto los estados como los operadores son matrices de números complejos, necesitando $2^{n}$ números complejos para los estados y $(2^{n})^{2}$ para los operadores. De aquí en adelante para simplificar se usará $N = 2^{n}$, siendo $n$ el número de qubits.
			
		En python los números complejos internamente se representan como dos valores en coma flotante, los cuales son implementados como un \textit{double} de C cada uno (habitualmente codificado en binario usando la codificación IEEE 754 de 64 bits).
			
		Los elementos principales donde se localiza el consumo de memoria son:
			
		\begin{itemize}
			\item La entrada, que es un estado cuántico de la librería de qutip. Internamente es una matriz columna de números complejos con $N$ filas.
				
			\item La salida, al igual que la entrada, es un estado cuántico de la librería de qutip, siendo internamente una matriz columna de números complejos con $N$ filas.
				
			\item El operador $F$ es una matriz cuadrada de rango $N$ de números complejos.
				
		\end{itemize}
			
		Tanto la entrada como la salida necesitan $N$ números complejos lo que supone un incremento lineal de memoria respecto a $N$, pero el operador en cambio, al ser una matriz cuadrada, son $N^{2}$ números complejos y esto es un incremento cuadrático, por lo tanto la complejidad espacial teórica para este algoritmo es 
		$$
			2N + N^{2} = 2 \times 2^{n} + (2^{n})^{2} = 2^{n+1} + 2^{2n}
		$$
		lo que supone una complejidad espacial exponencial respecto al número de qubits que se incrementa el doble de rápido para el operador que para el estado.
			
		Como puede comprobarse en la tabla la tabla \ref{tab:base_theo_min_mem_usage}, mientras que el incremento del uso de memoria de los estados es asumible, no lo es así el del operador.
			
		\begin{table}
			\centering
				
			\begin{tabular}{| c | c | c |}
	
				\hline
					
				\textbf{Qubits} & \textbf{Estado (\textit{complex})} & \textbf{Operador (\textit{complex})} \\ \hline \hline
				
				1 & 2 & 4 \\ \hline
				2 & 4 & 16 \\ \hline
				3 & 8 & 32 \\ \hline
				4 & 16 & 64 \\ \hline
				5 & 32 & 128 \\ \hline
				\vdots & \vdots & \vdots \\ \hline
				10 & $2^{10} =$ & $2^{20} =$ \\ 
				 & 1024 & 1.048.576 \\ \hline
				\vdots & \vdots & \vdots \\ \hline
				20 & $2^{20} =$ & $2^{40} =$ \\ 
				 & 1.048.576 & 1.099.511.627.776 \\ \hline
			\end{tabular}
				
			\caption{\textit{base\_impl} - Incremento mínimo teórico en el uso de memoria}
			\label{tab:base_theo_min_mem_usage}
		\end{table}
			
		En el supuesto de que se contasen con los 2GB íntegros para ser usados por el programa para el contenido de las variables, tendríamos  $2GB = 2 \times 2^{40} \times 8 = 2^{44}bits$, y puesto que cada número complejo utiliza 2 números en coma flotante y estos suelen necesitar 64 bits, cada número complejo consideramos que precisa de $1complex = 2float = 2^{9}bits$, y por lo tanto disponemos de espacio para $2^{44} / 2^{9} = 2^{35}$ números complejos. Esto hace un máximo almacenable de 17 qubits
		$$
			2 \times 2^{17} + (2^{17})^{2} = 2 \times 2^{17} + 2^{34} = 17.180.131.328 < 2^{35} = 34.359.738.368
		$$
		mientras que con 18 qubits ya se pasaría del espacio disponible solo con el operador 
		$$
			(2^{18})^{2} = 2^{36} > 2^{35}
		$$
		
		Los cálculos anteriores están realizados en el supuesto de disponer de 2GB enteros de memoria dedicados solo para el contenido de las variables, pero en la vida real la memoria es compartida entre el sistema operativo y los programas en ejecución. Ademas, aunque el proceso cuente con 2GB enteros, parte de estos se usarán para alojar el código a ser ejecutado, los indices con las direcciones de memoria dinámica, la pila de llamadas (stack) y el espacio para el contenido de las variables dinámicas (heap).
		
		El sistema operativo en reposo mantiene un uso aproximado de memoria RAM de medio GB (como puede observarse en la figura \ref{pic:base_memory_usage_standby}), lo cual reduce la memoria disponible aproximadamente a 1536 MB ($2^{43}+2^{42}$ bits, que son $2^{34}+2^{33}$ números complejos).
			
		\begin{figure}[ht!]
			\includegraphics[width=\textwidth]{base_implementation_memory_usage_standby}
			\centering
			\caption{\textit{base\_impl} - Consumo de recursos solo con el SO en ejecución}
			\label{pic:base_memory_usage_standby}
		\end{figure}
		
		Otro detalle a tener en cuenta es que en el algoritmo, el operador primero se construye como una matriz de la librería \textit{numpy}, para luego transformarse en un objeto de la librería cuántica de \textit{qutip}, por lo que en ese momento el operador se encuentra duplicado en memoria, aumentando el consumo real frente al consumo teórico mínimo calculado previamente. Así, en el momento de máximo uso de la memoria, se precisa de espacio para 
		$$
			2N + 2N^{2}
		$$
		números complejos (frente a los $2N + N^{2}$ calculados previamente).
			
		Con estas nuevas restricciones, el número máximo teórico de qubits antes de que haya problemas de memoria es de 16 qubits
		$$
			2 \times 2^{16} + 2 (2^{16})^{2} = 2 \times 2^{16} + 2 \times 2^{32} = 8.590.065.664 < 2^{34}+2^{33} = 25.769.803.776
		$$
		mientras que con 17 qubits ya se pasaría con el tamaño del operador en el momento que esta duplicado
		$$
			2 (2^{17})^{2} = 2 \times 2^{34} = 2^{35} > 2^{34}+2^{33}
		$$
		
	\subsection{Estudio de coste espacial - Práctico}	
			
		Pese a los cálculos teóricos, durante la ejecución del sistema hay más factores a tener en cuenta que consumen memoria, como son el propio shell desde el que se ejecuta el sistema, el interprete de python usado para la ejecución, el código propio y de librerías que ha de cargarse, el uso de espacio adicional de las estructuras de memoria utilizadas (por ejemplo un objeto de \textit{qutip} no es solo la matriz interna que lo representa) y el uso de memoria extra necesario para las operaciones. Todo esto conlleva que el número máximo de qubits con los que se puede trabajar antes de tener problemas de memoria es inferior al esperado.
		
		En concreto como puede verse en las figuras \ref{pic:base_memory_12_qubits} y \ref{pic:base_memory_full_break}, ejecutando el sistema con 12 qubits este se ejecuta satisfactoriamente, estando siendo usados un total de 1.9 GB de memoria RAM, pero al tratar de ejecutar el programa para 13 qubits, la librería de \textit{scipy} utilizada lanza una excepción debido a falta de memoria. Un análisis instrucción por instrucción del código demuestra que el punto donde salta esa excepción es la instrucción ``\textit{F = qutip.Qobj(F)}'' que es cuando se transforma la matriz de \textit{numpy} F con los valores del operador en un objeto operador de \textit{qutip}. Ese punto es también donde la ejecución con 12 qubits alcanzó el máximo uso de memoria.
			
		\begin{figure}[ht!]
			\includegraphics[width=\textwidth]{base_implementation_memory_12_qubits}
			\centering
			\caption{\textit{base\_impl} - Ejecución con 12 qubits}
			\label{pic:base_memory_12_qubits}
		\end{figure}
		
		\begin{figure}[ht!]
			\includegraphics[width=\textwidth]{base_implementation_memory_full_break}
			\centering
			\caption{\textit{base\_impl} - Ejecución con 13 qubits}
			\label{pic:base_memory_full_break}
		\end{figure}
		
		Esto puede observarse en la tabla \ref{tab:base_impl_resource_usage}. Como se puede observar, hay un mínimo consumo por parte del proceso de algo más de 42.41 MB, lo cual es suficiente hasta que se opera con 6 qubits, momento a partir del cual se incrementa muy rápido.
	
	\subsection{Estudio de coste temporal - Teórico}
		
		Así como el incremento en el uso de memoria crece de forma exponencial al número de qubits, también así lo hace el incremento en tiempo de ejecución, pues no solo ha de generarse el operador recorriendo una matriz cuadrada con la consiguiente complejidad cuadrática, sino que luego ha de realizarse una multiplicación de matrices al aplicar el operador al estado. 
		
		Por ello y puesto que el tamaño de las matrices es exponencial respecto al número de qubits, la complejidad se dispara, siendo la de construir del operador
		$$
			O(N^{2}) = O((2^{n})^{2}) = O(2^{2n})
		$$ 
		y la de aplicar el operador al estado
		$$
			O((2N)^{2}) = O((2(2^{n}))^{2}) = O((2^{n + 1})^{2}) = O(2^{2n + 2})
		$$
		lo que supone una complejidad total de ambas operaciones respecto al número de qubits ($n$) de
		$$
			O( 2^{2n} + 2^{2n + 2}) \approx O( 2^{2n} + 2^{2n}) \approx O(2 \times 2^{2n}) \approx O(2^{2n})
		$$
		esto es una complejidad cuadrática respecto al tamaño del vector ($N$) y por lo tanto exponencial respecto al número de qubits ($n$).

	\subsection{Estudio de coste temporal - Práctico}	
		
		Los tiempos de ejecución para distintos tamaños del estado pueden observarse en la tabla \ref{tab:base_impl_resource_usage}. Observando esos resultados se ve un incremento exponencial en el coste temporal del algoritmo, lo cual coincide con lo calculado teóricamente.


	\subsection{Conclusiones}
		
		El algoritmo ha sido implementado de forma efectiva, aunque dista mucho de ser eficiente. A nivel de memoria se ha conseguido una mejora significativa respecto a la implementación de \textit{qutip} como se ve en la tabla \ref{tab:mem_opt_memory_limits_comparation}, pero no se ha aumentado el límite superior al tamaño del estado (número de qubits) debido a una limitación por parte de la librería \textit{scipy}. 
		
		El consumo de memoria se concentra en el operador siendo este una matriz cuadrada y teniendo que estar duplicada en el momento de convertirla a un objeto de \textit{qutip} para operar con el estado de entrada. Se probó a construir el operador directamente sobre un objeto de \textit{qutip}, pero resulto en un gran aumento del tiempo de ejecución, por ejemplo al operar con un estado de 8 qubits pasa de tardar 0.05 segundos a casi 1 segundo, por lo que al ser una implementación inicial que será refinada en un futuro, se decidió seguir construyendo contenido del operador como una matriz aparte para luego transformarlo a un objeto de \textit{qutip}.
		
		En el aspecto temporal el consumo es considerable, y gran parte del tiempo se emplea en crear el operador, siendo necesario para cada posición de este una operación exponencial cuyo exponente es un número complejo.
		
		En comparación con los resultados de \textit{qutip}, no se ha conseguido aumentar el número de qubits operables pero si se ha reducido el consumo de memoria (por ejemplo de 1450 MB a 1131 MB para 12 qubits), aunque el coste temporal es más elevado siendo en el caso de 12 qubits cercano a 3/4 partes más que con la implementación de \textit{qutip} (pasa de casi 8 segundos con \textit{qutip} a casi 14).
		
		\begin{table}
			\centering
			
			\begin{tabular}{| c | c | c | c |}
				
				\hline
				
				\textbf{Qubits} & \textbf{Memoria (MB)} & \textbf{Tiempo (s)} & \textbf{Varianza (s)} \\ \hline \hline
				
				\input{tables/base_impl_time_measure}
				
			\end{tabular}
			
			\caption{\textit{base\_impl} - Resultados de las mediciones}
			\label{tab:base_impl_resource_usage}
		\end{table}