\section{\textit{mem\_opt\_1}: Operador parcial}
	
		Al analizar el operador, por sus propias características, no se ha encontrado ningún método de compresión de matrices adecuado para reducir su consumo de memoria (por ejemplo, no se pueden usar técnicas para matrices huecas pues el operador no contiene ceros). Por ello se decidió generar el operador fila por fila (en lugar de generarlo completamente) y operar cada fila por separado, con lo que el uso de memoria del operador se reduce de $N^{2}$ números complejos (matriz cuadrada) en todo momento, a $N$ números complejos (matriz fila) en cada momento concreto. Esto supone un aumento en el coste temporal del algoritmo, pero con una sustancial mejora espacial.
		
		A esta implementación se le ha asignado el identificador \textit{mem\_opt\_1}.
		
		En la tabla \ref{tab:mem_opt_1_resource_usage} se observan los resultados obtenidos de las mediciones al ejecutar el algoritmo con hasta 15 qubits y en las tablas \ref{tab:mem_opt_1_memory_limits_comparation} y \ref{tab:mem_opt_1_time_comparation} dos comparativas de memoria y tiempo con otras implementaciones.
		
		\subsection{Diseño}
		
			Partiendo de la implementación de \textit{base\_impl}, se han de modificar los bucles donde se genera el operador de forma que, mientras antes se generaba el operador completo para luego convertirlo aun objeto de \textit{qutip} y aplicarlo obteniendo el resultado completo, ahora se convierte a un objeto de \textit{qutip} y se aplica cada fila generada por separado, obteniendo el resultado fila a fila. Para aprovechar y reducir al máximo el consumo de memoria, las filas del operador se construyen sobre el mismo array de forma que el consumo de memoria sea constante durante la ejecución.

			Un diagrama de flujo del algoritmo con los cambios principales respecto a \textit{base\_impl} en cajas rojas puede observarse en la figura \ref{pic:mem_opt_1_flow_diagram}
			
			\begin{figure}[ht!]
				\includegraphics[width=\textwidth]{mem_opt_1_flow_diagram}
				\centering
				\caption{\textit{mem\_opt\_1} - Diagrama de flujo - Cambios}
				\label{pic:mem_opt_1_flow_diagram}
			\end{figure}

		\subsection{Estudio de coste espacial - Teórico}
		
			Al igual que en \textit{base\_impl}, el coste en memoria es dependiente del número de qubits, pero a diferencia de antes, el operador no es una matriz cuadrada, sino una matriz fila creada para cada valor del estado resultante. Dicha matriz fila contiene la misma cantidad de números complejos que cada uno de los estados (el de entrada y el resultante)
			
			Esto supone un consumo espacial teórico mínimo para este algoritmo de 
			$$
				4N = 4 \times 2^{n} = 2^{n+2}
			$$
			($2N$ de los estados de entrada y salida y $2N$ del operador duplicado al convertirlo a objeto de \textit{qutip}) frente al consumo mínimo de la implementación anterior que es 
			$$
				2N + 2N^{2} = 2(2^{n}) + 2 \times (2^{n})^{2} = 2^{n + 1} + 2^{2n + 1}
			$$
			para un número $n$ de qubits. 
			
			La mejora teórica es sustancial ($4N << 2N + 2N^{2}$) al haberse eliminado el termino cuadrático y quedando solo términos lineales respecto a $N$.
			
			Volviendo al supuesto de que se contasen con los 2GB íntegros para ser usados por el programa en el contenido de las variables, tendríamos  $2GB = 2 \times 2^{40} \times 8 = 2^{44}bits$, y puesto que cada número complejo utiliza 2 números en coma flotante y estos suelen necesitar 64 bits, cada número complejo consideramos que precisa de $2^{9}bits$ (2 float de C), y por lo tanto disponemos de espacio para $2^{44} / 2^{9} = 2^{35}$ números complejos. Esto hace un máximo teórico ideal de 33 qubits exactos
			$$
				4 \times 2^{33} = 2^{33 + 2} = 2^{35} = 34.359.738.368
			$$		
			mientras que con 34 qubits obviamente se pasaría		
			$$
				4 \times 2^{34} = 2^{34 + 2} = 2^{36} > 2^{35}
			$$
			
		\subsection{Estudio de coste espacial - Práctico}
		
			No obstante, y al igual que en \textit{base\_impl}, la situación real no es ideal, teniendo que tener en cuenta el consumo del propio SO, del programa, etc\dots 
			
			Puesto que los tiempo de ejecución del algoritmo completo para estados de más de 15 qubits se dispara, se ha ejecutado para estados de hasta 15 qubits de la misma forma que en la primera iteración y se ha medido el consumo de memoria del proceso. 
			
			En la tabla \ref{tab:mem_opt_1_resource_usage} puede observarse los resultados completos de la ejecución, y en la tabla \ref{tab:mem_opt_1_memory_limits_comparation} una comparativa con la implementación de partida. Se ha conseguido una enorme mejoría con respecto a los datos de \textit{base\_impl}. Ya no solo se ejecuta para estados de 13 qubits y más, sino que el consumo es inferior al mínimo del proceso hasta 11 qubits, incrementándose luego de forma mucho más lenta.	Como ejemplo \textit{base\_impl} precisa para 11 qubits 315.5 MB y para 12 qubits 1131.8 MB, mientras que esta implementación consume respectivamente solo 43.6 MB y 43.8 MB.
			
			En cuanto a los límites del algoritmo con la memoria disponible, se ha realizado una segunda implementación del algoritmo que permite calcular solo unos cuantos valores del vector resultado, pudiendo así comprobar como se comporta el consumo de memoria para estados de más de 15 qubits en un tiempo razonable. El consumo de memoria es igual para calcular el valor de una posición del estado resultado que para el resultado completo, pues se reutiliza el espacio reservado para el operador sobrescribiendo su contenido, y por lo tanto el consumo de memoria no crece una vez se ha calculado el primer valor. 
			
			Para comprobar que la medición del consumo es correcta, se han comparado que los valores resultantes son acordes con los obtenidos ejecutando el algoritmo completo para estados de 15 qubits y menos (teniendo en cuenta que diferentes ejecuciones del mismo programa pueden tener ligeras variaciones, como por ejemplo que el consumo mínimo del interprete de \textit{python} no siempre es el mismo).
			
			Como puede observarse en la tabla \ref{tab:mem_opt_memory_limits_comparation}, hasta 24 qubits el algoritmo se ejecuta, con un consumo máximo por parte del proceso de algo más de 1400 MB, no obstante cuando se ejecuta para un estado de 25 qubits se lanza una excepción \verb|MemoryError|. Esto se produce a la hora de aplicar el operador parcial al estado de entrada.
			
			\begin{table}
				
				\centering
				
				\begin{tabular}{| c | c | c |}
					
					\hline
					
					\textbf{n} & \textbf{base\_impl} & \textbf{mem\_opt\_1}  \\ \hline \hline
					
					\input{tables/mem_opt_1_memory_limits_comparation}
					
				\end{tabular}
				\caption{mem\_opt\_1 - Comparativa del uso de memoria (MB)}
				\label{tab:mem_opt_1_memory_limits_comparation}
			\end{table}
	
	
		\subsection{Estudio de coste temporal - Teórico}
				
			Al estudiar la distribución y contenido de los bucles, se ve para cada fila se genera el operador parcial ($O(N)$), se convierte a un objeto de qutip (como se hace una copia de la matriz $O(N)$) y se multiplican el estado y el operador (al ser una multiplicación del vector fila operador por el vector columna estado $O(N)$) lo cual da lugar a $O(3N)$, y esto se repite $N$ veces
			$$
				O(3N \times N) = O(3N^{2}) = O(3(2^{n})^{2}) = O(3(2^{2n})) \approx O(2^{2n})
			$$
			siendo una complejidad cuadrática respecto al tamaño del vector ($N$) y por lo tanto exponencial respecto al número de qubits ($n$).
			
			A grandes rasgos no hay cambio con \textit{base\_impl} cuya complejidad teórica es también cuadrática respecto a $N$ y exponencial respecto a $n$ ($O(2^{2n} + 2^{2n + 2}) \approx O(2^{2n})$). Esto se explica pues el número de bucles es el mismo solo que ordenados de diferente manera: mientras que en \textit{base\_impl} se generan todas las filas del operador, se convierten todas a la vez a un objeto qutip y se opera con todo a la vez, en esta implementación se genera, convierte y aplica cada fila por separado, pero al final en ambos se han generado, convertido y operado $N$ filas del operador.
			
			Aún así, el tiempo de ejecución de esta implementación se espera sea mayor que el de \textit{base\_impl} pues su término tiene un multiplicador aproximado de 2, mientras que el de esta implementación es 3
			$$
				O( 2^{2n} + 2^{2n + 2}) \approx O(2(2^{2n})) <  O(3(2^{2n}))
			$$
		
		\subsection{Estudio de coste temporal - Práctico}
		
			Como se esperaba y se observa en la tabla \ref{tab:mem_opt_1_time_comparation}, se obtuvo peores resultados que en \textit{base\_impl}, aumentando el tiempo de ejecución. Por ejemplo, para 11 qubits se incrementó el tiempo de 3.8 segundos en \textit{base\_impl} a 5.7 segundos y para 12 qubits de 14.7 segundos a 19 segundos.
			
			También se observa un aumento del consumo de memoria para estados de pocos qubits junto a un incremento menos acusado: el incremento entre 1 qubit y 9 de \textit{base\_impl} es de 0.00090 hasta 0.23258 (mas de 200 veces más para 9 qubits que para 1) mientras que en esta implementación aunque empieza ya en 0.02397 solo aumenta hasta 0.70011 (alrededor de 30 veces más)
			
			\begin{table}
				
				\centering
				
				\begin{tabular}{| c | c | c |}
					
					\hline
					
					\textbf{n} & \textbf{base\_impl} & \textbf{mem\_opt\_1}  \\ \hline \hline
					
					\input{tables/mem_opt_1_time_comparation}
					
				\end{tabular}
				\caption{mem\_opt\_1 - Comparativa del consumo de tiempo (s)}
				\label{tab:mem_opt_1_time_comparation}
			\end{table}
			
			
		\subsection{Conclusiones}
			
			Como se observa en la tabla \ref{tab:mem_opt_1_memory_limits_comparation}, el consumo de memoria se ha reducido drásticamente consiguiendo poder operar con hasta 24 qubits con un consumo máximo de 1427 MB, frente al límite de 12 qubits con un consumo de 1131 MB obtenidos con \textit{base\_impl}.
			
			En lo referente al coste temporal, el aumento respecto a \textit{base\_impl} que se observa en la tabla \ref{tab:mem_opt_1_time_comparation} es asumible, sobretodo teniendo en cuenta la reducción del consumo de memoria. Dicho coste temporal sigue centrado principalmente en la construcción del operador, aunque ahora sea un proceso fragmentado, debido a la gran cantidad de operaciones necesarias y a complejidad de estas.
			
			
			\begin{table}
				\centering
				
				\begin{tabular}{| c | c | c | c |}
					
					\hline
					
					\textbf{Qubits} & \textbf{Memoria (MB)} & \textbf{Tiempo (s)} & \textbf{Varianza (s)} \\ \hline \hline
					
					\input{tables/mem_opt_1_time_measure}
					
				\end{tabular}
				
				\caption{\textit{mem\_opt\_1} - Resultados de las mediciones}
				\label{tab:mem_opt_1_resource_usage}
			\end{table}
			
			
			