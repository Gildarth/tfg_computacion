\section{\textit{mem\_opt\_2}: No convertir el operador}
		
	Al observar el monitor del sistema durante la ejecución del algoritmo se ha detectado un pico de memoria considerablemente grande (el cual puede verse en la figura \ref{pic:mem_opt_2_operator_duplication_memory_usage}). Este pico de memoria se corresponde con el momento en el cual se convierte el operador generado a un objeto de \textit{qutip} para posteriormente aplicarlo al estado.
		
	\begin{figure}[ht!]
		\includegraphics[width=\textwidth]{base_implementation_operator_conversion}
		\centering
		\caption{\textit{mem\_opt\_2} - Memoria en el monitor de recursos (12 qubits)}
		\label{pic:mem_opt_2_operator_duplication_memory_usage}
	\end{figure}
	
	Por ello, se ha decidido realizar una variación de \textit{base\_impl} la cual omita esa parte de la conversión del operador a la que se le ha asignado el identificador \textit{mem\_opt\_2}.
	
	En la tabla \ref{tab:mem_opt_2_resource_usage} se observan los resultados obtenidos de las mediciones al ejecutar el algoritmo con hasta 15 qubits y en las tablas comparativas \ref{tab:mem_opt_memory_limits_comparation} y \ref{tab:mem_opt_time_usage_comparation} dos comparativas de memoria y tiempo con otras implementaciones.
	
	\subsection{Diseño}
	
		Para poder llevar a cabo estos cambios, se precisa obtener la representación matricial del estado de entrada para poder operar con las matrices directamente y luego convertir a objeto de \textit{qutip} solo la matriz resultado.
		
		Un diagrama de flujo del algoritmo con los cambios principales respecto a \textit{base\_impl} en cajas rojas puede observarse en la figura \ref{pic:mem_opt_2_flow_diagram}
		
		\begin{figure}[ht!]
			\includegraphics[width=\textwidth]{mem_opt_2_flow_diagram}
			\centering
			\caption{\textit{mem\_opt\_2} - Diagrama de flujo - Cambios}
			\label{pic:mem_opt_2_flow_diagram}
		\end{figure}
	
	\subsection{Estudio de coste espacial - Teórico}
	
		La principal diferencia respecto a \textit{base\_impl} radica en que, al no convertir el operador a un objeto de \textit{qutip} ates de aplicarlo, este ya no se encuentra duplicado en memoria. A cambio es la matriz resultado la que, antes de terminar, se encuentra duplicada en memoria. No obstante, la matriz resultado es de tamaño $N$ mientras que el operador es de tamaño $N^{2}$ y por lo tanto el consumo es muy inferior duplicando el resultado que duplicando el operador.
		
		Más concretamente, la complejidad espacial teórica es de
		$$
			3N + N^{2} = 3(2^{n}) + (2^{n})^{2} = 3(2^{n}) + 2^{2n} \approx 2^{n} + 2^{2n}
		$$
		mientras que la de \textit{base\_impl} era de $2N + 2N^{2}$. 
		
		Por ello, se espera una reducción del consumo de memoria cercana al 50\%, pero no un aumento en el número de qubits operables al no eliminarse el término cuadrático y por lo tanto la restricción de \textit{scipy} para operar con matrices de ese tamaño que surgió en \textit{base\_impl} para más de 12 qubits sigue presente.
		
	\subsection{Estudio de coste espacial - Práctico}
		
		Los resultados sobre el consumo de memoria para diferentes tamaños del estado de entrada se pueden observar en la tabla \ref{tab:mem_opt_2_resource_usage} y en las tablas comparativa \ref{tab:mem_opt_2_memory_limits_comparation} y \ref{tab:mem_opt_memory_limits_comparation}.
		
		Como se esperaba, a la hora de ejecutar el algoritmo para 13 qubits la librería \textit{scipy} lanza una excepción \verb|MemoryError| a la hora de realizar la multiplicación de matrices.
		
		También conforme a lo esperado, la memoria requerida se reduce de forma más visible a cuantos más qubits se operen, siendo de aproximadamente el 50\% para 12 qubits (de 1128 MB en \textit{base\_impl} a 554 MB). Ademas, como se observa en la figura \ref{pic:mem_opt_2_operator_duplication_memory_usage_new} el pico de memoria al final de la ejecución se ha eliminado.
	
		\begin{figure}[ht!]
			\includegraphics[width=\textwidth]{base_implementation_operator_conversion_new}
			\centering
			\caption{\textit{mem\_opt\_2} - Memoria en el monitor de recursos (12 qubits)}
			\label{pic:mem_opt_2_operator_duplication_memory_usage_new}
		\end{figure}
		
		\begin{table}
			
			\centering
			
			\begin{tabular}{| c | c | c |}
				
				\hline
				
				\textbf{n} & \textbf{base\_impl} & \textbf{mem\_opt\_2}  \\ \hline \hline
				
				\input{tables/mem_opt_2_memory_limits_comparation}
				
			\end{tabular}
			\caption{mem\_opt\_2 - Comparativa del uso de memoria (MB)}
			\label{tab:mem_opt_2_memory_limits_comparation}
		\end{table}
	
	\subsection{Estudio de coste temporal - Teórico}
		
		Puesto que el único cambio respecto a \textit{base\_impl} es la conversión a objeto de \textit{qutip}, la complejidad temporal sigue centrándose en la generación del operador y la aplicación de este al estado de entrada, implicando ambas operaciones el recorrido de una y dos matrices cuadradas respectivamente y por lo tanto la complejidad temporal teórica es exponencial respecto a $n$
		$$
			O(2^{2n} + 2^{2n + 2}) \approx O(2^{2n} + 2^{2n}) \approx O(2 \times 2^{2n}) \approx O(2^{2n})
		$$
		lo que resulta en una complejidad cuadrática respecto al tamaño del vector ($N$) y por lo tanto exponencial respecto al número de qubits ($n$).
	
	\subsection{Estudio de coste temporal - Práctico}
	
		Como puede observarse en la tabla \ref{tab:mem_opt_2_resource_usage} y en las tablas comparativas \ref{tab:mem_opt_2_time_comparation} y \ref{tab:mem_opt_time_usage_comparation}, pese a la eliminación del paso de conversión de la matriz operador a un objeto de \textit{qutip}, se ha aumentado ligeramente el tiempo de ejecución a partir de 8 qubits (por ejemplo para 12 qubits de una media de 14.7 a una de 15.7 segundos aproximadamente). 
		
		Puesto que las únicas diferencias son la generación de una representación matricial del estado de entrada, el operar con matrices directamente en lugar de objetos de \textit{qutip} y la conversión a objeto de \textit{qutip} solo del estado resultante, se deduce que la diferencia del tiempo de ejecución ha de estar en la extracción de la representación matricial del estado de entrada o en la aplicación del operador directamente con las matrices pues la conversión de matriz a objeto de \textit{qutip} ya se hacia antes y con un mayor volumen de datos. Ambas posibilidades consideradas tiene como origen la implementación interna que haga de sus objetos \textit{qutip}, pudiendo ser que sea muy costoso extraer una representación matricial de ellos o que se realice alguna optimización interna a la hora de aplicar un operador a un estado. 
		
		Haciendo pruebas sobre la ejecución del algoritmo y la librería se comprobó que el aumento de tiempo se debe, principalmente y con diferencia, a que durante la aplicación del operador al estado \textit{qutip} ha de hacer alguna optimización interna de la operación.
		
		\begin{table}
			
			\centering
			
			\begin{tabular}{| c | c | c |}
				
				\hline
				
				\textbf{n} & \textbf{base\_impl} & \textbf{mem\_opt\_2}  \\ \hline \hline
				
				\input{tables/mem_opt_2_time_comparation}
				
			\end{tabular}
			\caption{mem\_opt\_2 - Comparativa del consumo de tiempo (s)}
			\label{tab:mem_opt_2_time_comparation}
		\end{table}
	
	
	\subsection{Conclusiones}
		
		Como se observa en las tablas comparativas \ref{tab:mem_opt_2_memory_limits_comparation} y \ref{tab:mem_opt_2_time_comparation} el no convertir el operador a un objeto de \textit{qutip} en comparación a \textit{base\_impl} ha permitido reducir la memoria hasta un 50\% (de 1128 a 554 MB) para 12 qubits con un aumento en el tiempo de ejecución de 1 segundo (de 14.7 a 15.7 segundos). Teniendo en cuenta que lo que se trata de reducir es el consumo de memoria estos resultados se consideran positivos.
		
		Ademas, como se observa en la figura \ref{pic:mem_opt_2_operator_duplication_memory_usage_new}, el incremento del uso de memoria durante una ejecución es completamente lineal, sin sufrir ningún pico brusco hasta que acaba el algoritmo.
		
		No obstante, el objetivo principal (que es aumentar el número de qubits operables respecto a \textit{base\_impl}) no se ha conseguido, obteniendo un \verb|MemoryError| de la librería \textit{scipy} a la hora de operar con más de 12 qubits (que es tambien el límite de partida).		
		
		\begin{table}
			
			\centering
			
			\begin{tabular}{| c | c | c | c |}
				
				\hline
				
				\textbf{Qubits} & \textbf{Memoria (MB)} & \textbf{Tiempo (s)} & \textbf{Varianza (s)} \\ \hline \hline
				
				\input{tables/mem_opt_2_time_measure}
				
			\end{tabular}
			
			\caption{\textit{mem\_opt\_2} - Resultados de las mediciones}
			\label{tab:mem_opt_2_resource_usage}
		\end{table}