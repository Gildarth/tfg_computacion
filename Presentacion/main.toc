\beamer@endinputifotherversion {3.36pt}
\select@language {spanish}
\beamer@sectionintoc {1}{Introducci\IeC {\'o}n}{12}{0}{1}
\beamer@sectionintoc {2}{\IeC {\textquestiondown }Qu\IeC {\'e} es la computaci\IeC {\'o}n cu\IeC {\'a}ntica?}{19}{0}{2}
\beamer@sectionintoc {3}{Transformada de Fourier}{42}{0}{3}
\beamer@sectionintoc {4}{Etapa inicial}{72}{0}{4}
\beamer@sectionintoc {5}{base\_impl - Implementaci\IeC {\'o}n base}{75}{0}{5}
\beamer@sectionintoc {6}{Refinamiento de memoria}{76}{0}{6}
\beamer@subsectionintoc {6}{1}{mem\_opt\_1 - Operador parcial}{76}{0}{6}
\beamer@subsectionintoc {6}{2}{mem\_opt\_2 - No convertir el operador}{77}{0}{6}
\beamer@subsectionintoc {6}{3}{mem\_opt\_3 - Operador parcial y no convertir el operador}{78}{0}{6}
\beamer@subsectionintoc {6}{4}{Resultados}{79}{0}{6}
\beamer@sectionintoc {7}{Refinamiento de tiempo de ejecuci\IeC {\'o}n}{83}{0}{7}
\beamer@subsectionintoc {7}{1}{time\_opt\_1 - Reducci\IeC {\'o}n de c\IeC {\'a}lculos}{83}{0}{7}
\beamer@subsectionintoc {7}{2}{time\_opt\_2 - Ejecuci\IeC {\'o}n multithread}{84}{0}{7}
\beamer@subsectionintoc {7}{3}{time\_opt\_3 - Ejecuci\IeC {\'o}n multithread con c\IeC {\'a}lculos reducidos}{86}{0}{7}
\beamer@subsectionintoc {7}{4}{Resultados}{88}{0}{7}
\beamer@sectionintoc {8}{Resultados finales}{92}{0}{8}
\beamer@sectionintoc {9}{Conclusiones}{93}{0}{9}
